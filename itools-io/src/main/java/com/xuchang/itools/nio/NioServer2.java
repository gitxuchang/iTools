package com.xuchang.itools.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;


public class NioServer2 {


    public static void main(String[] args) throws IOException{

        //创建服务，绑定端口8080
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(8000));

        // channel 非阻塞
        serverSocketChannel.configureBlocking(false);

        // 创建Selector
        Selector selector = Selector.open();
        //把ServerSocketChannel注册到selector上，并且设置selector对客户端accept事件感兴趣
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        while (true){
            System.out.println("等待事件发生。。");
            // 返回就绪的channel，至少有一个就绪channel
            selector.select();

            //if(selector.select(3000)==0) continue;

            System.out.println("有事件发生了。。");

            // 拿到就绪的通道集合
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();

            while (iterator.hasNext()){

                SelectionKey selectionKey = iterator.next();
                //删除本次已处理的key，防止下次select重复处理
                iterator.remove();
                handle(selectionKey);

            }


        }

    }

    private static void handle(SelectionKey selectionKey) throws IOException {

        //连接事件
        if(selectionKey.isAcceptable()){
            System.out.println("有新的客户端连接了。。");
            ServerSocketChannel ssc = (ServerSocketChannel) selectionKey.channel();
            // 非阻塞
            SocketChannel socketChannel = ssc.accept();

            socketChannel.configureBlocking(false);
            // 设置通过Selector监听Channel时对读事件感兴趣
            socketChannel.register(selectionKey.selector(),SelectionKey.OP_READ);

        }else if(selectionKey.isReadable()){
            System.out.println("有客户端数据可读事件发生了。。");
            SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
            //缓存区
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

            int len = socketChannel.read(byteBuffer);
            if(len != -1){
                System.out.println("读取数据："+new String(byteBuffer.array(),0,len));
            }

            ByteBuffer bufferToWrite = ByteBuffer.wrap("HelloClient".getBytes());
            socketChannel.write(bufferToWrite);
            selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);

        }else if (selectionKey.isWritable()) {
            SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
            System.out.println("write事件");

            selectionKey.interestOps(SelectionKey.OP_READ);
            //socketChannel.close();
        }



    }
}
