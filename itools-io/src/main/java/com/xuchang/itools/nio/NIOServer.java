package com.xuchang.itools.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;


public class NIOServer {

    public static void main(String[] args) {


        List<SocketChannel> list = new ArrayList<>();
        // Channel
        try {
            //创建服务，绑定端口8080
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.bind(new InetSocketAddress(8000));

            // ServerSocketChannel 非阻塞
            serverSocketChannel.configureBlocking(false);

            while (true){
                // 监听客户端的连接
                SocketChannel socketChannel = serverSocketChannel.accept();// 非阻塞
                System.out.println("返回socketChannel："+socketChannel);
                if(socketChannel== null){
                    // 需要读取 已经连接的客户端发送的数据  socketChannel

                    for(SocketChannel sc: list){
                        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                        int len = sc.read(byteBuffer);
                        if(len >0){
                            System.out.println("读取数据："+new String(byteBuffer.array(),0,len));
                        }
                    }

                }else {
                    // 获取到了客户端连接
                    System.out.println("新的客户端建立连接"+socketChannel);
                    list.add(socketChannel);

                    // 设置非阻塞
                    socketChannel.configureBlocking(false);

                    ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
                    int len = socketChannel.read(byteBuffer);

                    if(len>0){
                        System.out.println("读取数据："+new String(byteBuffer.array(),0,len));
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
