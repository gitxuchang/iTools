package com.xuchang.itools.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2020-09-26 15:58
 */
public class FileOutputStreamTest {

    public void test1() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("D:/data/test/test.txt");
        fileOutputStream.write("hello world".getBytes("utf-8"));
        fileOutputStream.close();
    }
    public void test2() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("D:/data/test/test.txt");

        FileOutputStream fileOutputStream = new FileOutputStream("D:/data/test/test1.txt",true);
        int len;
        byte[] data = new byte[1024];
        while ((len = fileInputStream.read(data)) != -1){
            fileOutputStream.write(data,0,len);
        }

        fileInputStream.close();
        fileOutputStream.close();
    }

    public static void main(String[] args) {
        try {
            new FileOutputStreamTest().test2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
