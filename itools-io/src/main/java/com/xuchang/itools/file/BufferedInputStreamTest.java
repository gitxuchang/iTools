package com.xuchang.itools.file;

import java.io.*;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/25 17:31
 */
public class BufferedInputStreamTest {

    public void test1() throws IOException {
        File file = new File("D:\\data\\test\\note.7z");
        FileInputStream fileInputStream = new FileInputStream(file);

        // 核心代码
        int b;
        while ((b = fileInputStream.read()) != -1 ){
            System.out.print((char) b);
        }
    }
    public void test2() throws IOException {
        long start = System.currentTimeMillis();
        BufferedInputStream fis = new BufferedInputStream(new FileInputStream("D:\\data\\test\\note.7z"));
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream("D:\\data\\test\\note2.7z"));

        int len;
        byte[] data = new byte[8 * 1024];
        while ((len = fis.read(data)) != -1) {
            fos.write(data, 0, len);
        }

        System.out.println("拷贝耗时:"+ (System.currentTimeMillis() - start));
    }

    public static void main(String[] args) {
        try {
            new BufferedInputStreamTest().test2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
