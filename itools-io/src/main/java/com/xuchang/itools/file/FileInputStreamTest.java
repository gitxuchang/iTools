package com.xuchang.itools.file;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/25 17:31
 */
public class FileInputStreamTest {

    public void test1() throws IOException {
        File file = new File("D:/data/test/test.txt");
        FileInputStream fileInputStream = new FileInputStream(file);
        long startTime = System.currentTimeMillis();
        // 核心代码
        int b;
        while ((b = fileInputStream.read()) != -1 ){
            System.out.print((char) b);
        }
        fileInputStream.close();
        System.out.println("------执行时间--------------");
        System.out.println("运行时间"+(System.currentTimeMillis() - startTime));
    }

    public void test2() throws IOException {
        File file = new File("D:/data/test/test.txt");
        FileInputStream fileInputStream = new FileInputStream(file);
        long startTime = System.currentTimeMillis();
        // 核心代码
        byte[] data = new byte[2];
        while (fileInputStream.read(data) != -1 ){
            System.out.println(new String(data));
        }
        fileInputStream.close();
        System.out.println("------执行时间--------------");
        System.out.println("运行时间"+(System.currentTimeMillis() - startTime));
    }

    public void test3() throws IOException {
        File file = new File("D:/data/test/test.txt");
        FileInputStream fileInputStream = new FileInputStream(file);
        long startTime = System.currentTimeMillis();
        // 核心代码
        byte[] data = new byte[2];
        int len;
        while ( (len = fileInputStream.read(data)) != -1 ){
            System.out.print(new String(data,0,len));
        }
        fileInputStream.close();
        System.out.println("");
        System.out.println("------执行时间--------------");
        System.out.println("运行时间"+(System.currentTimeMillis() - startTime));
    }

    public static void main(String[] args) {
        try {
            new FileInputStreamTest().test3();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
