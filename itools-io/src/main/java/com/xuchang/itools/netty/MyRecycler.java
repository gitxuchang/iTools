package com.xuchang.itools.netty;

import io.netty.util.Recycler;


public class MyRecycler extends Recycler<User> {
    @Override
    protected User newObject(Handle<User> handle) {
        return new User(handle);
    }


    public static void main(String[] args) {
        MyRecycler recycler = new MyRecycler();
        // 从对象池获取对象
        User user = recycler.get();
        user.setName("fox");
        // 回收对象到对象池
        user.recycle();

        User user2 = recycler.get();
        System.out.println(user.equals(user2));
    }


}

class User{
    private String name;
    private Recycler.Handle handle;

    public User(Recycler.Handle handle) {
        this.handle = handle;
    }

    public void recycle(){
        //恢复出厂设置
        this.name= null;
        handle.recycle(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}