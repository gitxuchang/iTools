package com.xuchang.itools.netty;

import java.util.concurrent.atomic.AtomicInteger;


public class Test {
    public static void main(String[] args) {

        AtomicInteger idx = new AtomicInteger();

        for (int i = 0; i < 10; i++) {
            System.out.println(idx.getAndIncrement() & 3 - 1);
        }
    }
}
