package com.xuchang.patterns.behavioral.strategy.demo;

/**
 * @project: design_pattern
 * @description:
 * @author: XUCHANG
 * @create: 2021-04-18 20:34
 */
public interface DiscountStrategy {
    void onDiscount();
}
