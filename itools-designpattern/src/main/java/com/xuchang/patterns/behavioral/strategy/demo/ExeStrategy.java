package com.xuchang.patterns.behavioral.strategy.demo;

/**
 * @project: design_pattern
 * @description:
 * @author: XUCHANG
 * @create: 2021-04-18 20:38
 */
public class ExeStrategy {
    private DiscountStrategy discountStrategy;

    public ExeStrategy(DiscountStrategy discountStrategy) {
        this.discountStrategy = discountStrategy;
    }
    public void doExe(){
        discountStrategy.onDiscount();
    }
}
