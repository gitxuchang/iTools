package com.xuchang.patterns.behavioral.strategy.demo;

/**
 * @project: design_pattern
 * @description:
 * @author: XUCHANG
 * @create: 2021-04-18 20:35
 */
public class ThreeStrategy implements DiscountStrategy{
    @Override
    public void onDiscount() {
        System.out.println("three");
    }
}
