package com.xuchang.patterns.behavioral.strategy.demo;

/**
 * @project: design_pattern
 * @description:
 * @author: XUCHANG
 * @create: 2021-04-18 20:39
 */
public class Test {
    public static void main(String[] args) {
        ExeStrategy oneStrategy = new ExeStrategy(new OneStrategy());
        oneStrategy.doExe();
        ExeStrategy twoStrategy = new ExeStrategy(new TwoStrategy());
        twoStrategy.doExe();
        ExeStrategy threeStrategy = new ExeStrategy(new ThreeStrategy());
        threeStrategy.doExe();
    }
}
