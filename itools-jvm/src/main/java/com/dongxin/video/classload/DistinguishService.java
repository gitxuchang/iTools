package com.dongxin.video.classload;

public class DistinguishService
{
    /*
       input: param:初始化参数
       return: 0:成功  -1：失败
    * */
    public int init(DistinguishParam param)
    {
        mNativeObj = nativeInit(param);
        if(mNativeObj!=0)
        {
            return 0;
        }else
        {
            return -1;
        }
    }
    /*
        input: video:输入字符  inputImg:输入图像地址
        return: 识别结果
    * */
    public String recogniation(String video,long inputImg)
    {
        String recResult = nativeRec(mNativeObj, video, inputImg);

        return recResult;
    }

    static {
        //jni依赖dll
        String dirPath = System.getProperty("user.dir");

        //System.out.print("dirPath:"+dirPath +"\n");

        System.load(dirPath+"\\dll\\jsoncpp.dll");
        System.load(dirPath+"\\dll\\libiomp5md.dll");
        System.load(dirPath+"\\dll\\mkldnn.dll");
        System.load(dirPath+"\\dll\\mklml.dll");
        System.load(dirPath+"\\dll\\opencv_world440.dll");

        System.load(dirPath+"\\dll\\paddle_inference.dll");
        System.load(dirPath+"\\dll\\PackageRec.dll");

    }

    private  long mNativeObj = 0;

    public static native long nativeInit(DistinguishParam param);
    public static native String nativeRec(long thiz, String inputStr,long inputImg);
}