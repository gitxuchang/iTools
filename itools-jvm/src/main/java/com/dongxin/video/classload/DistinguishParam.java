package com.dongxin.video.classload;

public class DistinguishParam
{
    public String detModelPath;  //铁包检测算法model路径
    public String detBinPath;

    public String recModelPath;  //ocr识别算法model路径
    public String recBinPath;

    public String labelPath;  //ocr标签

    public float detScoreThreshold;  //铁包检测得分阈值
    public float recScoreThreshold;  //ocr得分阈值
    public float iouThreshold;  //重叠率阈值  判断铁包运动状态

    //deviceParam
    public boolean  useGPU;  //是否使用gpu
    public boolean  useFP16;  //是否使用FP16
    public boolean  useTensorrt;  //是否使用tensorrt
    public int gpuID;   //gpu id
    public int gpuMem; //gpu 显存
    public int cpuNumThreads;  //cpu线程数
    public boolean  useMkdnn; //是否使用mkdnn



}