package com.itools.jvm.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.WString;

public class TestDll1Service {
 public interface TestDll1 extends Library {
  TestDll1 INSTANCE = (TestDll1)Native.loadLibrary("libc_demo03", TestDll1.class);
  void Say();
  void Hello();
 }

 public static void main(String[] args) {
  TestDll1.INSTANCE.Say();
  TestDll1.INSTANCE.Hello();
 }
}