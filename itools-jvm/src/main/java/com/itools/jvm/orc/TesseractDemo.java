package com.itools.jvm.orc;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class TesseractDemo {

    public static void main(String[] args){

//        File imageFile = new File("D:\\data\\iTools\\itools-jvm\\000005.jpg");
//        File imageFile = new File("D:\\data\\iTools\\itools-jvm\\000004.jpg");
//        File imageFile = new File("D:\\data\\iTools\\itools-jvm\\000003.jpg");
        File imageFile = new File("C:\\Users\\xuchang\\Desktop\\01.png");
        ITesseract instance = new Tesseract();
        instance.setDatapath("D:\\app\\Tesseract-OCR\\tessdata");
        // 默认是英文（识别字母和数字），如果要识别中文(数字 + 中文），需要制定语言包
        instance.setLanguage("chi_sim");

        try{

            String result = instance.doOCR(imageFile).replace(" ","");
            System.out.println(result);
        }catch(TesseractException e){
            System.out.println(e.getMessage());
        }
    }
}
