package com.itools.jvm.jni;

//Test.java文件
public class Test2 {
    public static void main(String[] args){
        //创建JNI的对象call
        JniExample jniExample = new JniExample();
        //调用call()方法；
        String stringFromC = jniExample.getStringFromC();
        //输出调用后的结果i
        System.out.println("调用Java Native Interface,返回："+stringFromC);

//        //根据操作系统判断，如果是linux系统则加载c++方法库
//        String systemType = System.getProperty("os.name");
//        String ext = (systemType.toLowerCase().indexOf("win") != -1) ? ".dll" : ".so";
//        if (ext.equals(".so")) {
//            try {
//                NativeLoader.loader("native");
//            } catch (Exception e) {
//                System.out.println("加载so库失败");
//            }
//        }
//        System.out.println("loaded");
    }

}