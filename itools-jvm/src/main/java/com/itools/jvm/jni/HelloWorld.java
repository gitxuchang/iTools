package com.itools.jvm.jni;

class HelloWorld
{
	//native声明，用于生成c/c++代码
	public native void sayHelloWorld();
	
	//加载c/c++编译好的库
	static
	{
		System.loadLibrary("HelloWorld");
	}
	
	public static void main(String[] args)
	{
		new HelloWorld().sayHelloWorld();
	}
}