package com.itools.jvm.demo;

public class TestDynamicLoad {

    private final static String TEST = "TEST";
    static {
        System.out.println("*************load static TestDynamicLoad************");
    }

    public static void main(String[] args) {
        new A();
        System.out.println("*************load test************");
        B b = null;  //B不会加载，除非这里执行 new B()
    }
}

class A {
    private final static String TEST = "TEST";
    static {
        System.out.println("*************load A static ************");
    }

    public A() {
        System.out.println("*************initial A************");
    }
}

class B {
    static {
        System.out.println("*************load B static************");
    }

    public B() {
        System.out.println("*************initial B************");
    }
}
