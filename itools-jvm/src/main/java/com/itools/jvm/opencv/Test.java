package com.itools.jvm.opencv;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.net.URL;

import static org.opencv.highgui.HighGui.imshow;
import static org.opencv.highgui.HighGui.waitKey;
import static org.opencv.imgcodecs.Imgcodecs.imread;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
import static org.opencv.imgproc.Imgproc.COLOR_RGB2GRAY;
import static org.opencv.imgproc.Imgproc.cvtColor;

/**
 * @project: quality
 * @description:
 * @author: XUCHANG
 * @create: 2021-08-16 11:37
 */
public class Test {

    public static void main(String[] args) throws Exception {
        // 解决awt报错问题
        System.setProperty("java.awt.headless", "false");
        System.out.println(System.getProperty("java.library.path"));
        // 加载动态库
//        URL url = ClassLoader.getSystemResource("lib/opencv/opencv_java440.dll");
        URL url = ClassLoader.getSystemResource("opencv_java400.dll");
        System.load(url.getPath());
        // 读取图像
        String work_dir = System.getProperty("user.dir");
        String img_path = work_dir+"/002.jpg";
        Mat image = imread("D:\\data\\iTools\\itools-jvm\\002.jpg");
//        Mat image = imread(img_path);
        if (image.empty()) {
            throw new Exception("image is empty");
        }
        imshow("Original Image", image);

        // 创建输出单通道图像
        Mat grayImage = new Mat(image.rows(), image.cols(), CvType.CV_8SC1);
        // 进行图像色彩空间转换
        cvtColor(image, grayImage, COLOR_RGB2GRAY);

        imshow("Processed Image", grayImage);
        imwrite("D://hello.jpg", grayImage);
        waitKey();
    }
}
