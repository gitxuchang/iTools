-- 初始化脚本
CREATE TABLE IF NOT EXISTS `itools_init_db` (
             `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL,
             `CONTENT` longtext COLLATE utf8mb4_bin NOT NULL COMMENT '执行sql，一行，分隔符；',
             `TYPE` varchar(12) COLLATE utf8mb4_bin NOT NULL COMMENT '类型【ddl,dml,function】',
             `CREEATE_TIME` datetime NOT NULL COMMENT '创建时间',
             `VERSION` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '版本',
             `ENV` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分支环境',
             PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

