//package com.xuchang.itools.service;
//
//import com.netflix.zuul.ZuulFilter;
//import com.netflix.zuul.context.RequestContext;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpServletRequest;
//
///**
// * @author xuchang
// */
//@Component
//public class TestFilter extends ZuulFilter {
//    /**
//     * a "true" return from this method means that the run() method should be invoked
//     *
//     * @return true if the run() method should be invoked. false will not invoke the run() method
//     */
//    @Override
//    public boolean shouldFilter() {
//        return true;
//    }
//
//    //忽略无关代码，具体看github上的源码
//    @Override
//    public Object run() {
//        RequestContext ctx = RequestContext.getCurrentContext();
//        HttpServletRequest request = ctx.getRequest();
//        System.out.println("==============线程名称：" +                                         Thread.currentThread().getName()
//                + "，访问url：" + request.getRequestURI() + "================");
//        return null;
//    }
//
//    /**
//     * to classify a filter by type. Standard types in Zuul are "pre" for pre-routing filtering,
//     * "route" for routing to an origin, "post" for post-routing filters, "error" for error handling.
//     * We also support a "static" type for static responses see  StaticResponseFilter.
//     * Any filterType made be created or added and run by calling FilterProcessor.runFilters(type)
//     *
//     * @return A String representing that type
//     */
//    @Override
//    public String filterType() {
//        return null;
//    }
//
//    /**
//     * filterOrder() must also be defined for a filter. Filters may have the same  filterOrder if precedence is not
//     * important for a filter. filterOrders do not need to be sequential.
//     *
//     * @return the int order of a filter
//     */
//    @Override
//    public int filterOrder() {
//        return 0;
//    }
//}