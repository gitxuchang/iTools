package com.xuchang.itools.http;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 */
@WebServlet(name = "gateway", urlPatterns = "/zuul/*")
public class GatewayHttpServlet extends HttpServlet {

    private HttpRunner httpRunner = new HttpRunner();

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        System.out.println("GatewayServlet...");
        //将request，和response放入上下文对象中
        httpRunner.init(req, resp);
        try {
            //执行前置过滤
            httpRunner.preRoute();
            //执行过滤
            httpRunner.route();
            //执行后置过滤
            httpRunner.postRoute();
        } catch (Throwable e) {
            RequestContext.getCurrentContext().getResponse()
                    .sendError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
        } finally {
            //清除变量
            RequestContext.getCurrentContext().unset();
        }
    }

}