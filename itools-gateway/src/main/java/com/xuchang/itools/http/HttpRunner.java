package com.xuchang.itools.http;


import com.xuchang.itools.filter.GatewayFilter;
import com.xuchang.itools.filter.post.ResponseFilter;
import com.xuchang.itools.filter.pre.RequestWrapperFilter;
import com.xuchang.itools.filter.router.RouterFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 */
public class HttpRunner {
    /**
     * 过滤器
     */
    private ConcurrentHashMap<String, List<GatewayFilter>> hashFiltersByType = new ConcurrentHashMap<String, List<GatewayFilter>>(){{
        put("pre",new ArrayList<GatewayFilter>(){{
            add(new RequestWrapperFilter());
        }});
        put("route",new ArrayList<GatewayFilter>(){{
            add(new RouterFilter());
        }});
        put("post",new ArrayList<GatewayFilter>(){{
            add(new ResponseFilter());
        }});
    }};

    public void init(HttpServletRequest req, HttpServletResponse resp) {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.setRequest(req);
        ctx.setResponse(resp);
    }

    public void preRoute() throws Throwable {
        runFilters("pre");
    }

    public void route() throws Throwable{
        runFilters("route");
    }

    public void postRoute() throws Throwable{
        runFilters("post");
    }

    public void runFilters(String sType) throws Throwable {
        List<GatewayFilter> list = this.hashFiltersByType.get(sType);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                GatewayFilter zuulFilter = list.get(i);
                zuulFilter.run();
            }
        }
    }
}