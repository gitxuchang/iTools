package com.xuchang.itools.filter.router;

import com.xuchang.itools.filter.GatewayFilter;
import com.xuchang.itools.http.RequestContext;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 */
public class RouterFilter extends GatewayFilter {

    @Override
    public String filterType() {
        return "route";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public void run(){
        System.out.println("RoutingFilter..");
        RequestContext ctx = RequestContext.getCurrentContext();
        RequestEntity requestEntity = ctx.getRequestEntity();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity responseEntity = restTemplate.exchange(requestEntity,byte[].class);
        ctx.setResponseEntity(responseEntity);
    }
}
