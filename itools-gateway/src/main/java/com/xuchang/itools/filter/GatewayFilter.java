package com.xuchang.itools.filter;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 */
public abstract class GatewayFilter {
    abstract public String filterType();

    abstract public int filterOrder();

    abstract public void run();
}
