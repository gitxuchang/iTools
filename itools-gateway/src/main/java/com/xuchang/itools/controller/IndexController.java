package com.xuchang.itools.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-02-01 15:31
 */
@RestController
public class IndexController {
    @RequestMapping("/index")
    public String index(HttpServletRequest request) {
        return "测试请求转发，地址："+request.getRemoteHost();
    }
}
