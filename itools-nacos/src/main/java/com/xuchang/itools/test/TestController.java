package com.xuchang.itools.test;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-23 20:46
 */
@RestController
@RefreshScope
public class TestController {
    @NacosValue(value = "${server1.port}", autoRefreshed = true)
    String testProperties;

    @Value(value = "${server1.port}")
    String testProperties2;

    @GetMapping("/test")
    public String test(){
        System.out.println(testProperties);
        System.out.println(testProperties2);
        return testProperties+testProperties2;
    }
}
