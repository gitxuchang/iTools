package com.xuchang.itools;

import com.alibaba.nacos.api.config.annotation.NacosValue;
//import com.itools.core.snowflake.config.EnableSequenceService;
//import com.itools.core.validate.EnableValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableDiscoveryClient
@SpringBootApplication(/*scanBasePackages="com.itools.core"*/)
//@EnableSequenceService
//@EnableValidator
public class NacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosApplication.class, args);
    }


}