-- 20200927 初始化脚本
CREATE TABLE IF NOT EXISTS `itools_init_db` (
             `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL,
             `CONTENT` longtext COLLATE utf8mb4_bin NOT NULL COMMENT '执行sql，一行，分隔符；',
             `TYPE` varchar(12) COLLATE utf8mb4_bin NOT NULL COMMENT '类型【ddl,dml,function】',
             `CREEATE_TIME` datetime NOT NULL COMMENT '创建时间',
             `VERSION` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '版本',
             `ENV` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分支环境',
             PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `itools_application` (
              `ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
              `APP_NAME` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '系统名称',
              `APP_INSTANCE_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '系统唯一id',
              `APP_OWNER` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统所有者',
              `STATUS` smallint(6) NOT NULL COMMENT '状态',
              `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
              `CREATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
              `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '最近更新时间',
              `LAST_UPDATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最近更新人',
              `APP_CODE` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '系统编码',
              `APP_OWNER_EMAIL` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '系统所有者邮箱',
              `APP_REMARK` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
              `APP_TYPE` smallint(6) NOT NULL COMMENT '类型',
              PRIMARY KEY (`ID`),
              UNIQUE KEY `APP_CODE` (`APP_CODE`),
              UNIQUE KEY `APP_INSTANCE_ID` (`APP_INSTANCE_ID`),
              KEY `IDX_RES_APPLICATION` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `itools_application_auth_ref` (
                   `ID` varchar(32) NOT NULL,
                   `CREATE_TIME` bigint(20) NOT NULL,
                   `CREATE_USER_ID` varchar(32) NOT NULL,
                   `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
                   `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
                   `APPLICATION_ID` varchar(32) NOT NULL,
                   `AUTH_APP_ID` varchar(32) NOT NULL,
                   `STATUS` int(11) NOT NULL,
                   PRIMARY KEY (`ID`),
                   UNIQUE KEY `IDX_RES_APP_AUTH` (`APPLICATION_ID`,`AUTH_APP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `itools_application_logs` (
               `ID` varchar(32) NOT NULL,
               `CREATE_TIME` bigint(20) NOT NULL,
               `CREATE_USER_ID` varchar(32) NOT NULL,
               `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
               `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
               `APPLICATION_ID` varchar(32) NOT NULL,
               `AUTH_APP_IDS` longtext,
               `OPT_TYPE` varchar(6) DEFAULT NULL,
               `STATUS` int(11) NOT NULL,
               PRIMARY KEY (`ID`),
               KEY `IDX_RES_APP_LOGS` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `itools_application_provide` (
              `ID` varchar(32) NOT NULL,
              `CREATE_TIME` bigint(20) NOT NULL,
              `CREATE_USER_ID` varchar(32) NOT NULL,
              `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
              `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
              `APPLICATION_ID` varchar(32) NOT NULL,
              `SERVICE_CODE` varchar(32) NOT NULL,
              `ICON` varchar(64) DEFAULT NULL,
              `OPEN` smallint(6) DEFAULT NULL,
              `REMARK` varchar(255) DEFAULT NULL,
              `SERVICE_NAME` varchar(127) DEFAULT NULL,
              `SERVICE_PATH` varchar(255) DEFAULT NULL,
              `STATUS` smallint(6) NOT NULL,
              PRIMARY KEY (`ID`),
              KEY `IDX_RES_APP_PRIVIDE` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `itools_application_service` (
              `ID` varchar(32) NOT NULL,
              `CREATE_TIME` bigint(20) NOT NULL,
              `CREATE_USER_ID` varchar(32) NOT NULL,
              `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
              `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
              `APPLICATION_ID` varchar(32) NOT NULL,
              `SERVICE_NAME` varchar(255) NOT NULL,
              `SERVICE_CODE` varchar(32) NOT NULL,
              `REMARK` varchar(255) DEFAULT NULL,
              `STATUS` smallint(6) NOT NULL,
              PRIMARY KEY (`ID`),
              UNIQUE KEY `SERVICE_CODE` (`SERVICE_CODE`),
              KEY `IDX_RES_APP_SERVICE` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `itools_auth_resource_ref` (
                    `ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
                    `AUTHORIZATION_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '权限ID',
                    `RESOURCE_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '资源ID',
                    `TYPE` smallint(6) NOT NULL COMMENT '0：数据权限项，1：资源',
                    `STATUS` smallint(6) NOT NULL COMMENT '0：有效，1：超级管理员或父级置为失效，2：权限拥有者置为失效',
                    `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
                    `CREATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户',
                    `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
                    `LAST_UPDATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
                    `APPLICATION_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用ID',
                    PRIMARY KEY (`ID`),
                    UNIQUE KEY `IDX_RES_AUTH_RES` (`AUTHORIZATION_ID`,`RESOURCE_ID`,`TYPE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `itools_authorization` (
                `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
                `NAME` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
                `PARENT_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父ID',
                `REMARK` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
                `STATUS` smallint(6) NOT NULL COMMENT '状态',
                `CUST_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
                `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
                `CREATE_USER_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户',
                `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
                `LAST_UPDATE_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
                `APPLICATION_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用ID',
                `CODE` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'code',
                `ORDER_BY` smallint(6) DEFAULT NULL COMMENT '排序字段',
                PRIMARY KEY (`ID`),
                UNIQUE KEY `CODE` (`CODE`),
                KEY `IDX_RES_AUTHORIZATION` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `itools_enterprise` (
                 `ID` varchar(32) NOT NULL,
                 `CREATE_TIME` bigint(20) NOT NULL,
                 `CREATE_USER_ID` varchar(32) NOT NULL,
                 `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
                 `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
                 `EMPLOYEES_NUM` varchar(32) DEFAULT NULL,
                 `ENTER_ADDRESS` varchar(64) DEFAULT NULL,
                 `ENTER_CONTACTS` varchar(32) DEFAULT NULL,
                 `ENTER_NAME` varchar(64) DEFAULT NULL,
                 `ENTER_TRADE` varchar(32) DEFAULT NULL,
                 `ENTER_LICENSE` varchar(32) DEFAULT NULL,
                 PRIMARY KEY (`ID`),
                 KEY `IDX_RES_ENTERPRISE` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `itools_group` (
                `ID` varchar(32) NOT NULL,
                `CUST_ID` varchar(32) DEFAULT NULL,
                `CREATE_TIME` bigint(20) NOT NULL,
                `CREATE_USER_ID` varchar(32) NOT NULL,
                `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
                `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
                `APPLICATION_ID` varchar(32) DEFAULT NULL,
                `CODE` varchar(64) NOT NULL,
                `NAME` varchar(255) DEFAULT NULL,
                `REMARK` varchar(255) DEFAULT NULL,
                `STATUS` smallint(6) NOT NULL,
                PRIMARY KEY (`ID`),
                UNIQUE KEY `CODE` (`CODE`),
                KEY `IDX_RES_GROUP` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `itools_group_role_ref` (
                 `ID` varchar(32) NOT NULL,
                 `CREATE_TIME` bigint(20) NOT NULL,
                 `CREATE_USER_ID` varchar(32) NOT NULL,
                 `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
                 `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
                 `APPLICATION_ID` varchar(32) DEFAULT NULL,
                 `GROUP_ID` varchar(32) NOT NULL,
                 `ROLE_ID` varchar(32) NOT NULL,
                 PRIMARY KEY (`ID`),
                 UNIQUE KEY `IDX_RES_GROUP_ROLE` (`GROUP_ID`,`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `itools_group_user_ref` (
                 `ID` varchar(32) NOT NULL,
                 `CREATE_TIME` bigint(20) NOT NULL,
                 `CREATE_USER_ID` varchar(32) NOT NULL,
                 `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
                 `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
                 `APPLICATION_ID` varchar(32) DEFAULT NULL,
                 `GROUP_ID` varchar(32) NOT NULL,
                 `USER_ID` varchar(32) NOT NULL,
                 PRIMARY KEY (`ID`),
                 UNIQUE KEY `IDX_RES_GROUP_USER` (`GROUP_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `itools_organization` (
               `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
               `NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '机构名称',
               `PARENT_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父ID',
               `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
               `CREATE_USER_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户',
               `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
               `LAST_UPDATE_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
               `ADDRESS` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '地址',
               `APPLICATION_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用ID',
               `CODE` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT 'code',
               `CUST_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
               `FAX` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
               `LINK_MAN_DEPT` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人部门',
               `LINK_MAN_EMAIL` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人邮箱',
               `LINK_MAN_NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人名称',
               `LINK_MAN_POS` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
               `LINK_MAN_TEL` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人手机号',
               `RANK` smallint(6) DEFAULT '0' COMMENT '等级',
               `REMARK` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
               `STATUS` smallint(6) NOT NULL DEFAULT '0' COMMENT '状态',
               `TELEPHONE` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
               `TYPE` smallint(6) NOT NULL DEFAULT '0' COMMENT '0：经营单位；1：机构/部门',
               `ZIP_CODE` varchar(16) COLLATE utf8mb4_bin DEFAULT NULL,
               `PATH` varchar(1024) COLLATE utf8mb4_bin DEFAULT NULL,
               `ORGANIZATION_NUMBER` varchar(6) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '机构编号',
               `ADMIN_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '管理员id',
               `AREA` smallint(6) NOT NULL COMMENT '区域（0：区域，1：非区域）',
               `ORDER_BY` smallint(6) DEFAULT NULL COMMENT '排序字段',
               PRIMARY KEY (`ID`),
               UNIQUE KEY `CODE` (`CODE`),
               KEY `IDX_RES_ORGANIZATION` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
CREATE TABLE `itools_resource` (
                   `ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
                   `NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '资源名称',
                   `PARENT_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父ID',
                   `STATUS` smallint(6) NOT NULL COMMENT '状态',
                   `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
                   `CREATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户ID',
                   `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
                   `LAST_UPDATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
                   `APPLICATION_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用ID',
                   `CODE` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'code',
                   `ORDER_BY` smallint(6) DEFAULT NULL COMMENT '排序',
                   `PICTURE_NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图标',
                   `REMARK` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
                   `TYPE` smallint(6) NOT NULL COMMENT '类型',
                   `URL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由',
                   `SHOW_STATUS` smallint(6) DEFAULT NULL COMMENT '显示状态，0：显示，1：不显示',
                   `HAVE_MENU` smallint(6) DEFAULT NULL COMMENT '是否拥有，0：非拥有，1：拥有',
                   PRIMARY KEY (`ID`),
                   UNIQUE KEY `CODE` (`CODE`),
                   KEY `IDX_RES_RESOURCE` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `itools_role` (
               `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主键ID',
               `PARENT_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父ID',
               `NAME` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色名称',
               `CUST_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
               `APPLICATION_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '应用ID',
               `REMARK` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
               `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
               `CODE` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT 'code',
               `CREATE_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建用户',
               `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
               `LAST_UPDATE_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
               `STATUS` smallint(6) NOT NULL COMMENT '角色状态',
               `ORDER_BY` smallint(6) DEFAULT NULL COMMENT '排序字段',
               `TYPE` smallint(6) NOT NULL COMMENT '0：功能角色，1：数据角色',
               PRIMARY KEY (`ID`),
               UNIQUE KEY `CODE` (`CODE`),
               KEY `IDX_RES_ROLE` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `itools_role_authorization_ref` (
                 `ID` varchar(32) NOT NULL,
                 `CREATE_TIME` bigint(20) NOT NULL,
                 `CREATE_USER_ID` varchar(32) NOT NULL,
                 `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL,
                 `LAST_UPDATE_USER_ID` varchar(32) DEFAULT NULL,
                 `APPLICATION_ID` varchar(32) DEFAULT NULL,
                 `AUTHORIZATION_ID` varchar(32) NOT NULL,
                 `ROLE_ID` varchar(32) NOT NULL,
                 `STATUS` smallint(6) NOT NULL COMMENT '0：有效，1：超级管理员或父级置为失效，2：权限拥有者置为失效',
                 PRIMARY KEY (`ID`),
                 UNIQUE KEY `IDX_RES_ROLE_AUTH` (`ROLE_ID`,`AUTHORIZATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `itools_role_resource_ref` (
                `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID',
                `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
                `CREATE_USER_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户',
                `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
                `LAST_UPDATE_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
                `APPLICATION_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用ID',
                `ROLE_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
                `RESOURCE_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '资源ID',
                `STATUS` smallint(6) NOT NULL COMMENT '0：有效，1：超级管理员或父级置为失效，2：权限拥有者置为失效',
                `TYPE` smallint(6) NOT NULL COMMENT '0：数据权限项，1：资源',
                PRIMARY KEY (`ID`),
                UNIQUE KEY `IDX_RES_AUTH_RES` (`ROLE_ID`,`RESOURCE_ID`,`TYPE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
CREATE TABLE `itools_user` (
               `ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
               `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
               `CREATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户',
               `PLATFROM_USERID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
               `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
               `LAST_UPDATE_USER_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
               `COMPANY_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
               `ORGANIZATION_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
               `NAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
               `USER_TYPE` smallint(6) NOT NULL COMMENT '用户类型',
               `NODE_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
               `SEX` smallint(6) DEFAULT NULL COMMENT '性别',
               `BIRTHDAY` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '生日',
               `EMAIL` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
               `POSITION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '位置',
               `REMARK` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
               `TEL_PHONE` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
               `USER_CODE` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
               `USER_LEVEL` smallint(6) DEFAULT NULL,
               `FACE_URL` mediumblob COMMENT '头像',
               `PLAT_PERSON_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '工号',
               `SIGN` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
               `BRIEF_INTROD` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
               PRIMARY KEY (`ID`),
               UNIQUE KEY `USER_CODE` (`USER_CODE`),
               UNIQUE KEY `PLATFROM_USERID` (`PLATFROM_USERID`),
               UNIQUE KEY `TEL_PHONE_UK` (`TEL_PHONE`) USING BTREE,
               KEY `IDX_RES_USER` (`CREATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `itools_user_account` (
                   `ID` varchar(32) NOT NULL COMMENT '主键ID',
                   `ACCOUNT_EXPIRED` bigint(20) DEFAULT NULL,
                   `ACCOUNT_LOCKED` smallint(6) NOT NULL,
                   `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
                   `CREDENTIAL` varchar(512) DEFAULT NULL,
                   `CREDENTIAL_EXPIRED` bigint(20) DEFAULT NULL,
                   `IDENTIFIER` varchar(32) NOT NULL COMMENT '账号',
                   `IDENTITY_TYPE` smallint(6) NOT NULL COMMENT '账号类型 1:username,2:email,3:phone,4:wechat,5:qq',
                   `LAST_LOGIN_IP` varchar(15) DEFAULT NULL COMMENT '上次登录IP',
                   `LAST_LOGIN_TIME` varchar(32) DEFAULT NULL COMMENT '上次登录时间',
                   `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
                   `LOGIN_MODE` smallint(6) NOT NULL,
                   `STATUS` smallint(6) NOT NULL COMMENT '用户状态',
                   `USER_ID` varchar(32) NOT NULL COMMENT '用户ID',
                   `CREDENTIAL_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次密码修改时间',
                   `FIRST_PWD_ERROR_TIME` bigint(20) DEFAULT NULL COMMENT '第一次密码错误时间',
                   `LOGIN_PWD_ERROR_TIME` smallint(6) DEFAULT NULL COMMENT '登录密码错误的次数',
                   PRIMARY KEY (`ID`),
                   UNIQUE KEY `IDENTIFIER` (`IDENTIFIER`),
                   KEY `IDX_RES_ACCOUNT` (`LAST_UPDATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `itools_user_role_ref` (
                `ID` varchar(32) COLLATE utf8mb4_bin NOT NULL,
                `APPLICATION_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
                `CREATE_TIME` bigint(20) NOT NULL COMMENT '创建时间',
                `CREATE_USER_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '创建用户',
                `LAST_UPDATE_TIME` bigint(20) DEFAULT NULL COMMENT '上次修改时间',
                `LAST_UPDATE_USER_ID` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上次修改用户',
                `ROLE_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '角色ID',
                `USER_ID` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '用户ID',
                `TYPE` smallint(6) NOT NULL COMMENT '0：功能角色，1：数据角色',
                PRIMARY KEY (`ID`),
                UNIQUE KEY `IDX_RES_USER_ROLE` (`USER_ID`,`ROLE_ID`,`TYPE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;




