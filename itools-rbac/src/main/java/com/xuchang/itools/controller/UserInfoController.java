package com.xuchang.itools.controller;

import com.xuchang.itools.base.CommonResult;
import com.xuchang.itools.log.Logable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2020-09-28 21:04
 */
@RestController
@RequestMapping("user/info")
public class UserInfoController {


    @GetMapping("/test")
    @Logable
    public CommonResult test(){
        return CommonResult.success("test");
    }

    @GetMapping("auth/test1")
    @PreAuthorize("hasAuthority('user:add')")
    public String authTest1(){
        return "您拥有'user:add'权限";
    }


}
