package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.EnterpriseDTO;
import com.xuchang.itools.rbac.dto.EnterpriseDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnterpriseDTOMapper {
    long countByExample(EnterpriseDTOExample example);

    int deleteByExample(EnterpriseDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(EnterpriseDTO record);

    int insertSelective(EnterpriseDTO record);

    List<EnterpriseDTO> selectByExample(EnterpriseDTOExample example);

    EnterpriseDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EnterpriseDTO record, @Param("example") EnterpriseDTOExample example);

    int updateByExample(@Param("record") EnterpriseDTO record, @Param("example") EnterpriseDTOExample example);

    int updateByPrimaryKeySelective(EnterpriseDTO record);

    int updateByPrimaryKey(EnterpriseDTO record);
}