package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.UserAccountDTO;
import com.xuchang.itools.rbac.dto.UserAccountDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserAccountDTOMapper {
    long countByExample(UserAccountDTOExample example);

    int deleteByExample(UserAccountDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(UserAccountDTO record);

    int insertSelective(UserAccountDTO record);

    List<UserAccountDTO> selectByExample(UserAccountDTOExample example);

    UserAccountDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") UserAccountDTO record, @Param("example") UserAccountDTOExample example);

    int updateByExample(@Param("record") UserAccountDTO record, @Param("example") UserAccountDTOExample example);

    int updateByPrimaryKeySelective(UserAccountDTO record);

    int updateByPrimaryKey(UserAccountDTO record);
}