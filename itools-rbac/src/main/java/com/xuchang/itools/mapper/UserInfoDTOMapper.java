package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.UserInfoDTO;
import com.xuchang.itools.rbac.dto.UserInfoDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserInfoDTOMapper {
    long countByExample(UserInfoDTOExample example);

    int deleteByExample(UserInfoDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(UserInfoDTO record);

    int insertSelective(UserInfoDTO record);

    List<UserInfoDTO> selectByExampleWithBLOBs(UserInfoDTOExample example);

    List<UserInfoDTO> selectByExample(UserInfoDTOExample example);

    UserInfoDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") UserInfoDTO record, @Param("example") UserInfoDTOExample example);

    int updateByExampleWithBLOBs(@Param("record") UserInfoDTO record, @Param("example") UserInfoDTOExample example);

    int updateByExample(@Param("record") UserInfoDTO record, @Param("example") UserInfoDTOExample example);

    int updateByPrimaryKeySelective(UserInfoDTO record);

    int updateByPrimaryKeyWithBLOBs(UserInfoDTO record);

    int updateByPrimaryKey(UserInfoDTO record);
}