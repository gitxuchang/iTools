package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.AuthorizationDTO;
import com.xuchang.itools.rbac.dto.AuthorizationDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AuthorizationDTOMapper {
    long countByExample(AuthorizationDTOExample example);

    int deleteByExample(AuthorizationDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(AuthorizationDTO record);

    int insertSelective(AuthorizationDTO record);

    List<AuthorizationDTO> selectByExample(AuthorizationDTOExample example);

    AuthorizationDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") AuthorizationDTO record, @Param("example") AuthorizationDTOExample example);

    int updateByExample(@Param("record") AuthorizationDTO record, @Param("example") AuthorizationDTOExample example);

    int updateByPrimaryKeySelective(AuthorizationDTO record);

    int updateByPrimaryKey(AuthorizationDTO record);
}