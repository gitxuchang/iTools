package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.ApplicationProvideDTO;
import com.xuchang.itools.rbac.dto.ApplicationProvideDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApplicationProvideDTOMapper {
    long countByExample(ApplicationProvideDTOExample example);

    int deleteByExample(ApplicationProvideDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(ApplicationProvideDTO record);

    int insertSelective(ApplicationProvideDTO record);

    List<ApplicationProvideDTO> selectByExample(ApplicationProvideDTOExample example);

    ApplicationProvideDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ApplicationProvideDTO record, @Param("example") ApplicationProvideDTOExample example);

    int updateByExample(@Param("record") ApplicationProvideDTO record, @Param("example") ApplicationProvideDTOExample example);

    int updateByPrimaryKeySelective(ApplicationProvideDTO record);

    int updateByPrimaryKey(ApplicationProvideDTO record);
}