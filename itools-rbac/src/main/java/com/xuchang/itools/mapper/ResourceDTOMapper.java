package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.ResourceDTO;
import com.xuchang.itools.rbac.dto.ResourceDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ResourceDTOMapper {
    long countByExample(ResourceDTOExample example);

    int deleteByExample(ResourceDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(ResourceDTO record);

    int insertSelective(ResourceDTO record);

    List<ResourceDTO> selectByExample(ResourceDTOExample example);

    ResourceDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ResourceDTO record, @Param("example") ResourceDTOExample example);

    int updateByExample(@Param("record") ResourceDTO record, @Param("example") ResourceDTOExample example);

    int updateByPrimaryKeySelective(ResourceDTO record);

    int updateByPrimaryKey(ResourceDTO record);
}