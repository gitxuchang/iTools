package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.ApplicationInfoLogsDTO;
import com.xuchang.itools.rbac.dto.ApplicationInfoLogsDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApplicationInfoLogsDTOMapper {
    long countByExample(ApplicationInfoLogsDTOExample example);

    int deleteByExample(ApplicationInfoLogsDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(ApplicationInfoLogsDTO record);

    int insertSelective(ApplicationInfoLogsDTO record);

    List<ApplicationInfoLogsDTO> selectByExampleWithBLOBs(ApplicationInfoLogsDTOExample example);

    List<ApplicationInfoLogsDTO> selectByExample(ApplicationInfoLogsDTOExample example);

    ApplicationInfoLogsDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ApplicationInfoLogsDTO record, @Param("example") ApplicationInfoLogsDTOExample example);

    int updateByExampleWithBLOBs(@Param("record") ApplicationInfoLogsDTO record, @Param("example") ApplicationInfoLogsDTOExample example);

    int updateByExample(@Param("record") ApplicationInfoLogsDTO record, @Param("example") ApplicationInfoLogsDTOExample example);

    int updateByPrimaryKeySelective(ApplicationInfoLogsDTO record);

    int updateByPrimaryKeyWithBLOBs(ApplicationInfoLogsDTO record);

    int updateByPrimaryKey(ApplicationInfoLogsDTO record);
}