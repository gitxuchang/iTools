package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.AuthResourceDTO;
import com.xuchang.itools.rbac.dto.AuthResourceDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AuthResourceDTOMapper {
    long countByExample(AuthResourceDTOExample example);

    int deleteByExample(AuthResourceDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(AuthResourceDTO record);

    int insertSelective(AuthResourceDTO record);

    List<AuthResourceDTO> selectByExample(AuthResourceDTOExample example);

    AuthResourceDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") AuthResourceDTO record, @Param("example") AuthResourceDTOExample example);

    int updateByExample(@Param("record") AuthResourceDTO record, @Param("example") AuthResourceDTOExample example);

    int updateByPrimaryKeySelective(AuthResourceDTO record);

    int updateByPrimaryKey(AuthResourceDTO record);
}