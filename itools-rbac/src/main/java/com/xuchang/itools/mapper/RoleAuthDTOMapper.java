package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.RoleAuthDTO;
import com.xuchang.itools.rbac.dto.RoleAuthDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleAuthDTOMapper {
    long countByExample(RoleAuthDTOExample example);

    int deleteByExample(RoleAuthDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(RoleAuthDTO record);

    int insertSelective(RoleAuthDTO record);

    List<RoleAuthDTO> selectByExample(RoleAuthDTOExample example);

    RoleAuthDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") RoleAuthDTO record, @Param("example") RoleAuthDTOExample example);

    int updateByExample(@Param("record") RoleAuthDTO record, @Param("example") RoleAuthDTOExample example);

    int updateByPrimaryKeySelective(RoleAuthDTO record);

    int updateByPrimaryKey(RoleAuthDTO record);
}