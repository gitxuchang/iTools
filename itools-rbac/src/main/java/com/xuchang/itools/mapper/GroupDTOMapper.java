package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.GroupDTO;
import com.xuchang.itools.rbac.dto.GroupDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GroupDTOMapper {
    long countByExample(GroupDTOExample example);

    int deleteByExample(GroupDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(GroupDTO record);

    int insertSelective(GroupDTO record);

    List<GroupDTO> selectByExample(GroupDTOExample example);

    GroupDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GroupDTO record, @Param("example") GroupDTOExample example);

    int updateByExample(@Param("record") GroupDTO record, @Param("example") GroupDTOExample example);

    int updateByPrimaryKeySelective(GroupDTO record);

    int updateByPrimaryKey(GroupDTO record);
}