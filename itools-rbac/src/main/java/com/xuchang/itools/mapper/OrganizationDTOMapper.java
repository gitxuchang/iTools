package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.OrganizationDTO;
import com.xuchang.itools.rbac.dto.OrganizationDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrganizationDTOMapper {
    long countByExample(OrganizationDTOExample example);

    int deleteByExample(OrganizationDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(OrganizationDTO record);

    int insertSelective(OrganizationDTO record);

    List<OrganizationDTO> selectByExample(OrganizationDTOExample example);

    OrganizationDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OrganizationDTO record, @Param("example") OrganizationDTOExample example);

    int updateByExample(@Param("record") OrganizationDTO record, @Param("example") OrganizationDTOExample example);

    int updateByPrimaryKeySelective(OrganizationDTO record);

    int updateByPrimaryKey(OrganizationDTO record);
}