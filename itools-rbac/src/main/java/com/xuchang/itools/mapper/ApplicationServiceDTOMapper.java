package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.ApplicationServiceDTO;
import com.xuchang.itools.rbac.dto.ApplicationServiceDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApplicationServiceDTOMapper {
    long countByExample(ApplicationServiceDTOExample example);

    int deleteByExample(ApplicationServiceDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(ApplicationServiceDTO record);

    int insertSelective(ApplicationServiceDTO record);

    List<ApplicationServiceDTO> selectByExample(ApplicationServiceDTOExample example);

    ApplicationServiceDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ApplicationServiceDTO record, @Param("example") ApplicationServiceDTOExample example);

    int updateByExample(@Param("record") ApplicationServiceDTO record, @Param("example") ApplicationServiceDTOExample example);

    int updateByPrimaryKeySelective(ApplicationServiceDTO record);

    int updateByPrimaryKey(ApplicationServiceDTO record);
}