package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.RoleInfoDTO;
import com.xuchang.itools.rbac.dto.RoleInfoDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleInfoDTOMapper {
    long countByExample(RoleInfoDTOExample example);

    int deleteByExample(RoleInfoDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(RoleInfoDTO record);

    int insertSelective(RoleInfoDTO record);

    List<RoleInfoDTO> selectByExample(RoleInfoDTOExample example);

    RoleInfoDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") RoleInfoDTO record, @Param("example") RoleInfoDTOExample example);

    int updateByExample(@Param("record") RoleInfoDTO record, @Param("example") RoleInfoDTOExample example);

    int updateByPrimaryKeySelective(RoleInfoDTO record);

    int updateByPrimaryKey(RoleInfoDTO record);
}