package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.UserRoleDTO;
import com.xuchang.itools.rbac.dto.UserRoleDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserRoleDTOMapper {
    long countByExample(UserRoleDTOExample example);

    int deleteByExample(UserRoleDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(UserRoleDTO record);

    int insertSelective(UserRoleDTO record);

    List<UserRoleDTO> selectByExample(UserRoleDTOExample example);

    UserRoleDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") UserRoleDTO record, @Param("example") UserRoleDTOExample example);

    int updateByExample(@Param("record") UserRoleDTO record, @Param("example") UserRoleDTOExample example);

    int updateByPrimaryKeySelective(UserRoleDTO record);

    int updateByPrimaryKey(UserRoleDTO record);
}