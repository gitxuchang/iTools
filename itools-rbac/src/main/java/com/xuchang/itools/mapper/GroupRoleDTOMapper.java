package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.GroupRoleDTO;
import com.xuchang.itools.rbac.dto.GroupRoleDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GroupRoleDTOMapper {
    long countByExample(GroupRoleDTOExample example);

    int deleteByExample(GroupRoleDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(GroupRoleDTO record);

    int insertSelective(GroupRoleDTO record);

    List<GroupRoleDTO> selectByExample(GroupRoleDTOExample example);

    GroupRoleDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") GroupRoleDTO record, @Param("example") GroupRoleDTOExample example);

    int updateByExample(@Param("record") GroupRoleDTO record, @Param("example") GroupRoleDTOExample example);

    int updateByPrimaryKeySelective(GroupRoleDTO record);

    int updateByPrimaryKey(GroupRoleDTO record);
}