package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.ApplicationAuthDTO;
import com.xuchang.itools.rbac.dto.ApplicationAuthDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApplicationAuthDTOMapper {
    long countByExample(ApplicationAuthDTOExample example);

    int deleteByExample(ApplicationAuthDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(ApplicationAuthDTO record);

    int insertSelective(ApplicationAuthDTO record);

    List<ApplicationAuthDTO> selectByExample(ApplicationAuthDTOExample example);

    ApplicationAuthDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ApplicationAuthDTO record, @Param("example") ApplicationAuthDTOExample example);

    int updateByExample(@Param("record") ApplicationAuthDTO record, @Param("example") ApplicationAuthDTOExample example);

    int updateByPrimaryKeySelective(ApplicationAuthDTO record);

    int updateByPrimaryKey(ApplicationAuthDTO record);
}