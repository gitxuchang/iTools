package com.xuchang.itools.mapper;


import com.xuchang.itools.rbac.initDB.InitDBInsertDTO;
import com.xuchang.itools.rbac.initDB.InitDBQueryDTO;
import com.xuchang.itools.rbac.initDB.InitDBResultDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/11 9:56
 */
@Repository
public interface InitDBMapper {

    List<InitDBResultDTO> query(InitDBQueryDTO dbQuery);

    Integer multipartInsert(@Param("dbInsertParams") List<InitDBInsertDTO> dbInsertParams);
}
