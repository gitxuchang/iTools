package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.RoleResourceDTO;
import com.xuchang.itools.rbac.dto.RoleResourceDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleResourceDTOMapper {
    long countByExample(RoleResourceDTOExample example);

    int deleteByExample(RoleResourceDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(RoleResourceDTO record);

    int insertSelective(RoleResourceDTO record);

    List<RoleResourceDTO> selectByExample(RoleResourceDTOExample example);

    RoleResourceDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") RoleResourceDTO record, @Param("example") RoleResourceDTOExample example);

    int updateByExample(@Param("record") RoleResourceDTO record, @Param("example") RoleResourceDTOExample example);

    int updateByPrimaryKeySelective(RoleResourceDTO record);

    int updateByPrimaryKey(RoleResourceDTO record);
}