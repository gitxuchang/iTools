package com.xuchang.itools.mapper;

import com.xuchang.itools.rbac.dto.ApplicationInfoDTO;
import com.xuchang.itools.rbac.dto.ApplicationInfoDTOExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApplicationInfoDTOMapper {
    long countByExample(ApplicationInfoDTOExample example);

    int deleteByExample(ApplicationInfoDTOExample example);

    int deleteByPrimaryKey(String id);

    int insert(ApplicationInfoDTO record);

    int insertSelective(ApplicationInfoDTO record);

    List<ApplicationInfoDTO> selectByExample(ApplicationInfoDTOExample example);

    ApplicationInfoDTO selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ApplicationInfoDTO record, @Param("example") ApplicationInfoDTOExample example);

    int updateByExample(@Param("record") ApplicationInfoDTO record, @Param("example") ApplicationInfoDTOExample example);

    int updateByPrimaryKeySelective(ApplicationInfoDTO record);

    int updateByPrimaryKey(ApplicationInfoDTO record);
}