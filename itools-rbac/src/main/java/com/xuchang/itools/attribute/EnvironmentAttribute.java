//package com.xuchang.itools.attribute;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.stereotype.Component;
//
///**
// * @project: iTools
// * @description:
// * @author: XUCHANG
// * @create: 2020-09-28 22:43
// */
//@Component
//public class EnvironmentAttribute {
//    @Autowired
//    private Environment environment;
//
//    private String ddlProperty = environment.getProperty("initDB.ddl");
//
//    private String delimiter = environment.getProperty("initDB.delimiter");
//}
