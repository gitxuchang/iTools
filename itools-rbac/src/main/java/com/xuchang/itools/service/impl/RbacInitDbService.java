package com.xuchang.itools.service.impl;

import com.xuchang.itools.bean.BeanCopyUtils;
import com.xuchang.itools.init.InitDbServiceAware;
import com.xuchang.itools.system.AbstractService;
import com.xuchang.itools.mapper.InitDBMapper;
import com.xuchang.itools.rbac.initDB.InitDBInsertDTO;
import com.xuchang.itools.rbac.initDB.InitDBQueryDTO;
import com.xuchang.itools.rbac.initDB.InitDBResultDTO;
import com.xuchang.itools.rbac.param.initDB.InitDBInsertParam;
import com.xuchang.itools.rbac.param.initDB.InitDBQueryParam;
import com.xuchang.itools.rbac.param.initDB.InitDBResult;
import com.xuchang.itools.init.InitDbService;
import com.xuchang.itools.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2020-09-28 22:52
 */
public class RbacInitDbService extends InitDbServiceAware {
    @Autowired
    private InitDBMapper initDBMapper;
    @Override
    public List<InitDBResult> query(InitDBQueryParam dbQuery) {
        InitDBQueryDTO initDBQuery = new InitDBQueryDTO();
        BeanCopyUtils.copyProperties(dbQuery,initDBQuery);
        List<InitDBResultDTO> initDBResult = initDBMapper.query(initDBQuery);
        List<InitDBResult> results = BeanCopyUtils.copy(initDBResult,InitDBResult.class);
        return results;
    }

    @Override
    public Integer multipartInsert(List<InitDBInsertParam> dbInsertParams) {
        if (CollectionUtils.isEmpty(dbInsertParams)){
            return 0;
        }
        List<InitDBInsertDTO> dbInserts = new ArrayList<>(dbInsertParams.size());
        Date now = new Date();
        dbInsertParams.forEach(dbInsertParam -> {
            InitDBInsertDTO dbInsertDTO = new InitDBInsertDTO();
            BeanCopyUtils.copyProperties(dbInsertParam,dbInsertDTO);
            dbInsertDTO.setId(this.createGeneralCode());
            dbInsertDTO.setCreateTime(now);
            dbInserts.add(dbInsertDTO);
        });
        return initDBMapper.multipartInsert(dbInserts);
    }
}
