package com.xuchang.itools.domain;

import lombok.Data;

import java.util.Date;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-28 15:51
 */
@Data
public class SysRole implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    private Long parentId;

    private String name;

    private String enname;

    private String description;

    private Date created;

    private Date updated;

}
