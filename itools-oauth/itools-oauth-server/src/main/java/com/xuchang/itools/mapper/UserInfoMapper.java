package com.xuchang.itools.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.xuchang.itools.domain.SysUser;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-29 13:57
 */
@Repository
public interface UserInfoMapper extends BaseMapper<SysUser> {

    @Select("select * from tb_user where username=#{username}")
    SysUser getByUsername(String username);
}
