package com.xuchang.itools.domain;

import lombok.Data;

import java.util.Date;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-28 15:51
 */
@Data
public class SysUser implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    private String password;

    private String phone;

    private String email;

    private Date created;

    private Date updated;


}
