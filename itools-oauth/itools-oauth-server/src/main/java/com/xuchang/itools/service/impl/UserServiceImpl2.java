package com.xuchang.itools.service.impl;


import com.xuchang.itools.domain.SysPermission;
import com.xuchang.itools.domain.SysUser;
import com.xuchang.itools.mapper.PermissionMapper;
import com.xuchang.itools.mapper.UserInfoMapper;
import com.xuchang.itools.service.UserService2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-29 14:00
 */
@Service("userDetailsService")
public class UserServiceImpl2 implements UserService2 {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public SysUser getByUsername(String username) {
        return userInfoMapper.getByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = getByUsername(username);
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (user==null){
            throw new UsernameNotFoundException("用户名不存在");
        }
        List<SysPermission> permissions = permissionMapper.selectByUserId(user.getId());

        permissions.forEach(permission -> {
            if (permission!=null && !StringUtils.isEmpty(permission.getEnname())){
                GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(permission.getEnname());
                authorities.add(grantedAuthority);
            }
        });
        return new User(user.getUsername(),user.getPassword(),authorities);
    }

}
