package com.xuchang.itools.service.impl;

import com.xuchang.itools.domain.SysPermission;
import com.xuchang.itools.mapper.PermissionMapper;
import com.xuchang.itools.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-29 13:58
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<SysPermission> selectByUserId(Long userId) {

        return permissionMapper.selectByUserId(userId);
    }
}
