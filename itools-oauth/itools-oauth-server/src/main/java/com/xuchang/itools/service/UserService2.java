package com.xuchang.itools.service;

import com.xuchang.itools.domain.SysUser;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-29 13:57
 */
public interface UserService2 extends UserDetailsService {

    SysUser getByUsername(String username);
}
