package com.xuchang.itools.service;


import com.xuchang.itools.domain.SysPermission;

import java.util.List;

/**
 * @project: itools-backend
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-29 13:57
 */
public interface PermissionService  {

    List<SysPermission> selectByUserId(Long userId);
}
