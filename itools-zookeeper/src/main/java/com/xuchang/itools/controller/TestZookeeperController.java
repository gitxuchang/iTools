package com.xuchang.itools.controller;

import com.xuchang.itools.config.ZookeeperServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-01-09 15:05
 */
@RestController
public class TestZookeeperController {

    @Autowired
    ZookeeperServer zookeeperServer;

    @RequestMapping("/test")
    public String event() throws Exception {
        zookeeperServer.watchEvent();
        return "success";
    }
}