package com.xuchang.itools.service.impl;

import com.xuchang.itools.base.CommonResult;
import com.xuchang.itools.service.ZookeeperCrudService;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-01-09 15:06
 */
@Service
public class ZookeeperCrudServiceImpl implements ZookeeperCrudService {
    @Autowired
    private ZooKeeper zooKeeper;
    /***
     * 创建持久节点
     * 配色整特
     * @param path
     * @param data
     * @return
     */
    @Override
    public CommonResult<String> createPersistent(String path, String data) {
        try {
            String result = zooKeeper.create(path, data.getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            return CommonResult.success(result);
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return  null;
    }

    /***
     * 创建临时节点
     * 因法美楼
     * 银困受SEQUENTIAL
     * @param path
     * @param data
     * @return
     */
    @Override
    public CommonResult<String> createEphemeral(String path, String data) {
        return null;
    }

    /**
     * 获取节点数据
     *
     * @param path
     * @return
     */
    @Override
    public CommonResult<String> getData(String path) {
        return null;
    }

    /**
     * 更新信息
     *
     * @param path
     * @param data
     * @return
     */
    @Override
    public CommonResult<String> setData(String path, String data) {
        return null;
    }

    /**
     * 是否存在
     *
     * @param path
     * @return
     */
    @Override
    public CommonResult<Stat> exists(String path) {
        return null;
    }

    /**
     * 删除
     *
     * @param path
     * @return
     */
    @Override
    public CommonResult<Boolean> delete(String path) {
        return null;
    }

    /**
     * 删除节点及其子节点
     *
     * @param path
     * @return
     */
    @Override
    public CommonResult<Boolean> deleteRecursive(String path) {
        return null;
    }
}
