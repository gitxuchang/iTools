package com.xuchang.itools.service;

import com.xuchang.itools.base.CommonResult;
import org.apache.zookeeper.data.Stat;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-01-09 15:05
 */
public interface ZookeeperCrudService {
    /***
     * 创建持久节点
     * 配色整特
     * @param path
     * @param data
     * @return
     */
    CommonResult<String> createPersistent(String path,String data);

    /***
     * 创建临时节点
     * 因法美楼
     * 银困受SEQUENTIAL
     * @param path
     * @param data
     * @return
     */
    CommonResult<String> createEphemeral(String path,String data);

    /**
     * 获取节点数据
     * @param path
     * @return
     */
    CommonResult<String> getData(String path);

    /**
     * 更新信息
     * @param path
     * @param data
     * @return
     */
    CommonResult<String> setData(String path,String data);

    /**
     * 是否存在
     * @param path
     * @return
     */
    CommonResult<Stat> exists(String path);

    /**
     * 删除
     * @param path
     * @return
     */
    CommonResult<Boolean> delete(String path);

    /**
     * 删除节点及其子节点
     * @param path
     * @return
     */
    CommonResult<Boolean> deleteRecursive(String path);
}
