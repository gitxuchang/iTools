package com.xuchang.itools.config;

import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.CountDownLatch;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-01-09 15:05
 */
@Configuration
public class ZookeeperConfig {

    private final static Logger logger = LoggerFactory.getLogger(ZookeeperConfig.class);

    @Value("${zookeeper.address}")
    private String address;

    @Value("${zookeeper.timeout}")
    private int timeout;

    @Bean
    public ZooKeeper getZookeeper(){
        ZooKeeper zooKeeper = null;
        try {
            /**
             * CountDownLatch 用于标记线程是否执行完。
             */
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            zooKeeper = new ZooKeeper(address, timeout, (x) -> {
                if(Watcher.Event.KeeperState.SyncConnected == x.getState()){
                    countDownLatch.countDown();
                }
            });
            countDownLatch.await();
            logger.info("zookeeper连接成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zooKeeper;
    }
}