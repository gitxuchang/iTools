package com.xuchang.itools.config;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-01-09 15:05
 */
@Component
public class ZookeeperServer {

    @Autowired
    ZooKeeper zooKeeper;

    /**
     * 监听其中的一个节点
     * @throws KeeperException
     * @throws InterruptedException
     */
    public void watchEvent() throws KeeperException, InterruptedException {
        Stat stat = new Stat();
        zooKeeper.getData("/node", (x)-> {
            System.out.println(x.getType());
        }, stat);
    }
}