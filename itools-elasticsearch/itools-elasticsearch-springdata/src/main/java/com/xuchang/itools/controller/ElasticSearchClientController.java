package com.xuchang.itools.controller;

import com.xuchang.itools.entity.Item;
import com.xuchang.itools.service.ElasticSearchClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-07-06 13:54
 */
@RestController
@Slf4j
public class ElasticSearchClientController {
    @Autowired
    private ElasticSearchClientService elasticSearchClientService;

    @PostMapping("createIndex")
    public boolean createIndex() {
        return elasticSearchClientService.createIndex();
    }

    @GetMapping("addItems")
    public boolean addItems() {
        return elasticSearchClientService.addItems();
    }
}
