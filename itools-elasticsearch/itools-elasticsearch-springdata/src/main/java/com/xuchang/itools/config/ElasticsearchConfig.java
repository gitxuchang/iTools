package com.xuchang.itools.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ElasticsearchConfig {
	@Value("${elasticsearch.host}")
	private String host;

	@Value("${elasticsearch.port}")
	private int port;

	@Value("${elasticsearch.connTimeout}")
	private int connTimeout;

	@Value("${elasticsearch.socketTimeout}")
	private int socketTimeout;

	@Value("${elasticsearch.connectionRequestTimeout}")
	private int connectionRequestTimeout;

	@Value("${elasticsearch.username}")
	private String USERNAME;

	@Value("${elasticsearch.password}")
	private String PASSWORD;


	@Bean(destroyMethod = "close", name = "client")
	public RestHighLevelClient initRestClient() {

		//如果没配置密码就可以不用下面这两部
//		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
//		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(USERNAME, PASSWORD));

		RestClientBuilder builder = RestClient.builder(new HttpHost(host, port))
				.setRequestConfigCallback(requestConfigBuilder -> requestConfigBuilder
				.setConnectTimeout(connTimeout)
				.setSocketTimeout(socketTimeout)
				.setConnectionRequestTimeout(connectionRequestTimeout));
		//没有密码可以不用这一个set
//                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//			@Override
//			public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
//				httpClientBuilder.disableAuthCaching();
//				return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
//			}
//		});

		return new RestHighLevelClient(builder);
	}
}
