package com.xuchang.itools.service;

import com.xuchang.itools.entity.Item;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-07-06 14:11
 */
public interface ElasticSearchClientService {
    boolean createIndex();

    boolean addItems();
}
