package com.xuchang.itools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @project: iTools
 * @description:
 * Elasticsearch JAVA操作有4种客户端:
 * 1、TransportClient
 * 2、JestClient
 * 3、RestClient
 * 4、spring-data-elasticsearch
 *
 * 特征：
 * 支持Spring的基于@Configuration的java配置方式，或者XML配置方式
 * 提供了用于操作ES的便捷工具类ElasticsearchTemplate。包括实现文档到POJO之间的自动智能映射。
 * 利用Spring的数据转换服务实现的功能丰富的对象映射
 * 基于注解的元数据映射方式，而且可扩展以支持更多不同的数据格式
 * 根据持久层接口自动生成对应实现方法，无需人工编写基本操作代码（类似mybatis，根据接口自动得到实现）。当然，也支持人工定制查询
 *
 * ElasticsearchTemplate在对应的springboot2.2版本之后废弃；springboot2.2支持es7+
 * 官方将在未来的8.0版本后移除之前的api 推荐使用RestHighLevelClient
 * @author: XUCHANG
 * @create: 2021-06-26 12:38
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ElasticSearchSpringDataApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElasticSearchSpringDataApplication.class, args);
    }
}
