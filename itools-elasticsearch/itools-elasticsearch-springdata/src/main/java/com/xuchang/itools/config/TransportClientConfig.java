//package com.xuchang.itools.config;
//
//import org.elasticsearch.client.Client;
//import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.common.settings.Settings;
//import org.elasticsearch.common.transport.TransportAddress;
//import org.elasticsearch.transport.client.PreBuiltTransportClient;
//import org.springframework.context.annotation.Bean;
//import org.springframework.data.elasticsearch.config.ElasticsearchConfigurationSupport;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
//
//import java.net.InetAddress;
//
///**
// * @project: iTools
// * @description:
// * @author: XUCHANG
// * @create: 2021-07-06 15:19
// */
//public class TransportClientConfig extends ElasticsearchConfigurationSupport {
//
//    @Bean
//    public Client elasticsearchClient() throws Exception{
//        Settings settings = Settings.builder().put("cluster.name", "elasticsearch").build();
//        TransportClient transportClient = new PreBuiltTransportClient(settings);
//        transportClient.addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
//        return transportClient;
//    }
//
//    @Bean(name = {"elasticsearchTemplate","elasticsearchOperations"})
//    public ElasticsearchTemplate elasticsearchTemplate() throws Exception{
//        return new ElasticsearchTemplate(elasticsearchClient());
//    }
//}
