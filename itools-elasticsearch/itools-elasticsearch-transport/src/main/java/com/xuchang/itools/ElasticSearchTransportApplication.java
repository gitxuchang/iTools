package com.xuchang.itools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @project: iTools
 * @description:
 * Elasticsearch JAVA操作有4种客户端:
 * 1、TransportClient
 * 2、JestClient
 * 3、RestClient
 * 4、spring-data-elasticsearch
 * @author: XUCHANG
 * @create: 2021-06-26 12:38
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ElasticSearchTransportApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElasticSearchTransportApplication.class, args);
    }
}
