package com.xuchang.itools.config;

import java.util.ArrayList;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback;
import org.elasticsearch.client.RestClientBuilder.RequestConfigCallback;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticsearchConfig {
	/**
	 * 集群地址，多个用,隔开
	 */
	private static String hosts = "127.0.0.1";
	/**
	 * 使用的端口号
	 */
	private static int port = 9200;
	/**
	 * 使用的协议
	 */
	private static String schema = "http";
	private static ArrayList<HttpHost> hostList = null;
	/**
	 * 连接超时时间
	 */
	private static int connectTimeOut = 1000;
	/**
	 * 连接超时时间
	 */
	private static int socketTimeOut = 30000;
	/**
	 * 获取连接的超时时间
	 */
	private static int connectionRequestTimeOut = 500;
	/**
	 * 最大连接数
	 */
	private static int maxConnectNum = 100;
	/**
	 * 最大路由连接数
	 */
	private static int maxConnectPerRoute = 100;
	 
	static {
		hostList = new ArrayList<>();
		String[] hostStrs = hosts.split(",");
		for (String host : hostStrs) {
			hostList.add(new HttpHost(host, port, schema));
		}
	}
	 
	@Bean
	public RestHighLevelClient client() {
		RestClientBuilder builder = RestClient.builder(hostList.toArray(new HttpHost[0]));
		// 异步httpclient连接延时配置
		builder.setRequestConfigCallback(new RequestConfigCallback() {
			@Override
			public Builder customizeRequestConfig(Builder requestConfigBuilder) {
				requestConfigBuilder.setConnectTimeout(connectTimeOut);
				requestConfigBuilder.setSocketTimeout(socketTimeOut);
				requestConfigBuilder.setConnectionRequestTimeout(connectionRequestTimeOut);
				return requestConfigBuilder;
			}
		});
		// 异步httpclient连接数配置
		builder.setHttpClientConfigCallback(new HttpClientConfigCallback() {
			@Override
			public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
				httpClientBuilder.setMaxConnTotal(maxConnectNum);
				httpClientBuilder.setMaxConnPerRoute(maxConnectPerRoute);
				return httpClientBuilder;
			}
		});
		RestHighLevelClient client = new RestHighLevelClient(builder);
		return client;
	}
//	@Bean(destroyMethod = "close", name = "client")
//	public RestHighLevelClient initRestClient() {
//
//		//如果没配置密码就可以不用下面这两部
//		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
//		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(USERNAME, PASSWORD));
//
//		RestClientBuilder builder = RestClient.builder(new HttpHost(host, port))
//				.setRequestConfigCallback(requestConfigBuilder -&gt; requestConfigBuilder
//				.setConnectTimeout(connTimeout)
//				.setSocketTimeout(socketTimeout)
//				.setConnectionRequestTimeout(connectionRequestTimeout))
//		//没有密码可以不用这一个set
//                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//			@Override
//			public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
//				httpClientBuilder.disableAuthCaching();
//				return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
//			}
//		});
//
//		return new RestHighLevelClient(builder);
//	}
}
