package com.xuchang.itools.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xuchang.itools.entity.EventInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 13:37
 */
@Component
public class DirectConsumer {
    private static final Logger log= LoggerFactory.getLogger(DirectConsumer.class);

    @Autowired
    public ObjectMapper objectMapper;

    /**
     * 监听并消费队列中的消息-directExchange-one
     */
    @RabbitListener(queues = "${mq.direct.queue.one.name}",containerFactory = "singleListenerContainer")
    public void consumeDirectMsgOne(@Payload byte[] msg){
        try {
            EventInfo info=objectMapper.readValue(msg, EventInfo.class);
            log.info("消息模型directExchange-one-消费者-监听消费到消息：{} ",info);


        }catch (Exception e){
            log.error("消息模型directExchange-one-消费者-监听消费发生异常：",e.fillInStackTrace());
        }
    }

    /**
     * 监听并消费队列中的消息-directExchange-two
     */
    @RabbitListener(queues = "${mq.direct.queue.two.name}",containerFactory = "singleListenerContainer")
    public void consumeDirectMsgTwo(@Payload byte[] msg){
        try {
            EventInfo info=objectMapper.readValue(msg, EventInfo.class);
            log.info("消息模型directExchange-two-消费者-监听消费到消息：{} ",info);


        }catch (Exception e){
            log.error("消息模型directExchange-two-消费者-监听消费发生异常：",e.fillInStackTrace());
        }
    }
}
