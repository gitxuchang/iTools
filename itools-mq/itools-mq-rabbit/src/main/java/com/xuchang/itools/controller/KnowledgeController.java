package com.xuchang.itools.controller;

import com.xuchang.itools.entity.KnowledgeInfo;
import com.xuchang.itools.publisher.KnowledgeManualPublisher;
import com.xuchang.itools.publisher.KnowledgePublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 14:47
 */
@RestController
@RequestMapping("/knowledge")
public class KnowledgeController {

    @Autowired
    private KnowledgePublisher knowledgePublisher;
    @Autowired
    private KnowledgeManualPublisher knowledgeManualPublisher;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        KnowledgeInfo info=new KnowledgeInfo();
        info.setId(10010);
        info.setCode("auto");
        info.setMode("基于AUTO的消息确认消费模式");

        knowledgePublisher.sendAutoMsg(info);
    }

    @RequestMapping("/test2")
    public void test2() throws Exception{
        KnowledgeInfo info=new KnowledgeInfo();
        info.setId(10011);
        info.setCode("manual");
        info.setMode("基于MANUAL的消息确认消费模式");

        knowledgeManualPublisher.sendAutoMsg(info);
    }
}
