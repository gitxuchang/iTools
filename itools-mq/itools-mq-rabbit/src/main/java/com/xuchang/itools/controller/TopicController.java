package com.xuchang.itools.controller;

import com.xuchang.itools.publisher.TopicPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 14:09
 */
@RestController
@RequestMapping("/topic")
public class TopicController {
    @Autowired
    private TopicPublisher topicPublisher;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        String msg="这是TopicExchange消息模型的消息";

        //此时相当于*，即java替代了*的位置;
        //当然由于#表示任意个单词，故而也将路由到#表示的路由和对应的队列中
        String routingKeyOne="local-xuchang.middleware.mq.topic.routing.java.key";
        //此时相当于#：即 php.python 替代了#的位置
        String routingKeyTwo="local-xuchang.middleware.mq.topic.routing.php.python.key";
        //此时相当于#：即0个单词
        String routingKeyThree="local-xuchang.middleware.mq.topic.routing.key";

        topicPublisher.sendMsgTopic(msg,routingKeyOne);
        topicPublisher.sendMsgTopic(msg,routingKeyTwo);
        topicPublisher.sendMsgTopic(msg,routingKeyThree);
    }
}
