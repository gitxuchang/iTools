package com.xuchang.itools.controller;

import com.xuchang.itools.entity.Person;
import com.xuchang.itools.publisher.BasicPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 11:00
 */
@RestController
@RequestMapping("/basic")
public class BasicController {
    @Autowired
    private BasicPublisher basicPublisher;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        String msg="~~~~这是一串字符串消息~~~~";
        basicPublisher.sendMsg(msg);
    }

    @RequestMapping("/test2")
    public void test2() throws Exception{
        Person p=new Person(1,"大圣","debug");
        basicPublisher.sendObjectMsg(p);
    }
}
