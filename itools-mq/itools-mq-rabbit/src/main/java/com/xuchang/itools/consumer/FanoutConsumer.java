package com.xuchang.itools.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xuchang.itools.entity.EventInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 11:34
 */
@Component
public class FanoutConsumer {

    private static final Logger log= LoggerFactory.getLogger(FanoutConsumer.class);

    @Autowired
    public ObjectMapper objectMapper;

    /**
     * 监听并消费队列中的消息-fanoutExchange-one
     */
    @RabbitListener(queues = "${mq.fanout.queue.one.name}",containerFactory = "singleListenerContainer")
    public void consumeFanoutMsgOne(@Payload byte[] msg){
        try {
            EventInfo info=objectMapper.readValue(msg, EventInfo.class);
            log.info("消息模型fanoutExchange-one-消费者-监听消费到消息：{} ",info);


        }catch (Exception e){
            log.error("消息模型-消费者-发生异常：",e.fillInStackTrace());
        }
    }

    /**
     * 监听并消费队列中的消息-fanoutExchange-two
     */
    @RabbitListener(queues = "${mq.fanout.queue.two.name}",containerFactory = "singleListenerContainer")
    public void consumeFanoutMsgTwo(@Payload byte[] msg){
        try {
            EventInfo info=objectMapper.readValue(msg, EventInfo.class);
            log.info("消息模型fanoutExchange-two-消费者-监听消费到消息：{} ",info);


        }catch (Exception e){
            log.error("消息模型-消费者-发生异常：",e.fillInStackTrace());
        }
    }
}
