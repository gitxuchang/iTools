package com.xuchang.itools.controller;

import com.xuchang.itools.entity.EventInfo;
import com.xuchang.itools.publisher.FanoutPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 11:36
 */
@RestController
@RequestMapping("/fanout")
public class FanoutController {
    @Autowired
    private FanoutPublisher fanoutPublisher;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        EventInfo info=new EventInfo(1,"增删改查模块","基于fanoutExchange的消息模型","这是基于fanoutExchange的消息模型");
        fanoutPublisher.sendMsgFanout(info);
    }
}
