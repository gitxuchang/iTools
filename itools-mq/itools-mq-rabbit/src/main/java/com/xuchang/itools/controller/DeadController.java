package com.xuchang.itools.controller;

import com.xuchang.itools.entity.DeadInfo;
import com.xuchang.itools.publisher.DeadPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 14:27
 */
@RestController
@RequestMapping("/dead")
public class DeadController {
    @Autowired
    private DeadPublisher deadPublisher;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        //定义实体对象1
        DeadInfo info=new DeadInfo(1,"~~~~我是第一则消息~~~");
        //发送实体对象1消息入死信队列
        deadPublisher.sendMsg(info);
        Thread.sleep(10000);
        //定义实体对象2
        info=new DeadInfo(2,"~~~~我是第二则消息~~~");
        //发送实体对象2消息入死信队列
        deadPublisher.sendMsg(info);

        //等待30s再结束-目的是为了能看到消费者监听真正队列中的消息
        Thread.sleep(20000);
    }
}
