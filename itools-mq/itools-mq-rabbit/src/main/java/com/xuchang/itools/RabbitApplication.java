package com.xuchang.itools;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-05-25 13:34
 */
@SpringBootApplication
public class RabbitApplication {
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(RabbitApplication.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);
    }

}
