package com.xuchang.itools.publisher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xuchang.itools.entity.EventInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 11:36
 */
@Component
public class FanoutPublisher {
    private static final Logger log= LoggerFactory.getLogger(FanoutPublisher.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Environment env;

    /**
     * 发送消息-基于FanoutExchange消息模型
     * @param info
     */
    public void sendMsgFanout(EventInfo info){
        if (info!=null){
            try {
                rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
                rabbitTemplate.setExchange(env.getProperty("mq.fanout.exchange.name"));

                Message msg= MessageBuilder.withBody(objectMapper.writeValueAsBytes(info)).build();
                rabbitTemplate.convertAndSend(msg);

                log.info("消息模型fanoutExchange-生产者-发送消息：{} ",info);
            }catch (Exception e){
                log.error("消息模型fanoutExchange-生产者-发送消息发生异常：{} ",info,e.fillInStackTrace());
            }
        }
    }
}
