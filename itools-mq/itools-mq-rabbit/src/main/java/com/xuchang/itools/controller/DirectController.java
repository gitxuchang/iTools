package com.xuchang.itools.controller;

import com.xuchang.itools.entity.EventInfo;
import com.xuchang.itools.publisher.DirectPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 13:39
 */
@RestController
@RequestMapping("/direct")
public class DirectController {
    @Autowired
    private DirectPublisher directPublisher;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        EventInfo info=new EventInfo(1,"增删改查模块-1","基于directExchange消息模型-1","directExchange-1");
        directPublisher.sendMsgDirectOne(info);

        info=new EventInfo(2,"增删改查模块-2","基于directExchange消息模型-2","directExchange-2");
        directPublisher.sendMsgDirectTwo(info);
    }
}
