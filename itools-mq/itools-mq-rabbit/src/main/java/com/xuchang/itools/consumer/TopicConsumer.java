package com.xuchang.itools.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 14:08
 */
@Component
public class TopicConsumer {
    private static final Logger log= LoggerFactory.getLogger(FanoutConsumer.class);

    @Autowired
    public ObjectMapper objectMapper;

    /**
     * 监听并消费队列中的消息-topicExchange-*通配符
     */
    @RabbitListener(queues = "${mq.topic.queue.one.name}",containerFactory = "singleListenerContainer")
    public void consumeTopicMsgOne(@Payload byte[] msg){
        try {
            String message=new String(msg,"utf-8");
            log.info("消息模型topicExchange-*-消费者-监听消费到消息：{} ",message);


        }catch (Exception e){
            log.error("消息模型topicExchange-*-消费者-监听消费发生异常：",e.fillInStackTrace());
        }
    }


    /**
     * 监听并消费队列中的消息-topicExchange-#通配符
     */
    @RabbitListener(queues = "${mq.topic.queue.two.name}",containerFactory = "singleListenerContainer")
    public void consumeTopicMsgTwo(@Payload byte[] msg){
        try {
            String message=new String(msg,"utf-8");
            log.info("消息模型topicExchange-#-消费者-监听消费到消息：{} ",message);


        }catch (Exception e){
            log.error("消息模型topicExchange-#-消费者-监听消费发生异常：",e.fillInStackTrace());
        }
    }
}
