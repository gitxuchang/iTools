package com.xuchang.itools.controller;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-07 15:02
 */
@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("/test1")
    public void test1() throws Exception{
        RabbitAdmin rabbitAdmin = new RabbitAdmin(rabbitTemplate.getConnectionFactory());

        DirectExchange directExchange = new DirectExchange("direct1");
        rabbitAdmin.declareExchange(directExchange);

        Queue queue = new Queue("queue1");
        rabbitAdmin.declareQueue(queue);

        Binding binding = BindingBuilder.bind(queue).to(directExchange).with("routing-key1");
        rabbitAdmin.declareBinding(binding);
    }
}
