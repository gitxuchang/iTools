package com.xuchang.itools.controller;

import com.xuchang.itools.service.SpringProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-05-25 10:23
 */
@RestController
@RequestMapping("/rocket")
public class RocketTestController {
    @Autowired
    private SpringProducer springProducer;
    @RequestMapping("/test1")
    public void test1(){
        springProducer.sendMessage("TestTopic","test mq msg");
    }

    @RequestMapping("/test2")
    public void test2() throws InterruptedException {
        springProducer.sendMessageInTransaction("TestTopic","test mq msg");
    }
}
