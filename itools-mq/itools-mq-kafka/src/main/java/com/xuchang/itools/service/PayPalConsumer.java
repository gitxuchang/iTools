//package com.xuchang.itools.service;
//
///**
// * get msg from kafka
// */
//@Component
//public class PayPalConsumer implements MessageListener<String, String> {
//
//    private static Logger logger = LoggerFactory.getLogger(PayPalConsumer.class);
//    @Autowired
//    private XXService XXService;
//    @Override
//    public void onMessage(ConsumerRecord<String, String> authorizeRecord) {
//        String value = authorizeRecord.value();
//        if (StringUtils.isEmpty(value)){
//            logger.warn("receive message from kafka is null");
//            return;
//        }
//        logger.info("receive message from kafka is {}",value);
//    }
//}