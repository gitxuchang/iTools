package com.xuchang.itools.controller;

import com.xuchang.itools.service.KafkaProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-05-25 10:23
 */
@RestController
@RequestMapping("/kafka")
public class KafkaTestController {
    @Autowired
    private KafkaProviderService kafkaProviderService;

    @RequestMapping("/test1")
    public void test1(){
        kafkaProviderService.sendMsg();
    }

    @RequestMapping("/test2")
    public void test2(){
        kafkaProviderService.test2();
    }
}
