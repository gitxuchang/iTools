package com.xuchang.itools.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaCustomer {
    @KafkaListener(topics = {"kafka-spring"})
    public void listen(ConsumerRecord<?, ?> record, Acknowledgment ack) {
        try {
            log.info("自行确认方式收到消息的key: " + record.key());
            log.info("自行确认方式收到消息的value: " + record.value().toString());
          ack.acknowledge();
        } finally {
            log.info("消息确认！");
           
        }
    }
    /**
     * @KafkaListener(groupId = "testGroup", topicPartitions = {
     *             @TopicPartition(topic = "topic1", partitions = {"0", "1"}),
     *             @TopicPartition(topic = "topic2", partitions = "0",
     *                     partitionOffsets = @PartitionOffset(partition = "1", initialOffset = "100"))
     *     },concurrency = "6")
     *  //concurrency就是同组下的消费者个数，就是并发消费数，必须小于等于分区总数
     * @param record
     */
    @KafkaListener(topics = "my-replicated-topic",groupId = "Group2")
    public void listenZhugeGroup(ConsumerRecord<String, String> record, Acknowledgment ack) {
        log.info("组{}自行确认方式收到消息的key: {}" ,"Group2",record.key());
        log.info("组{}自行确认方式收到消息的value: {}","Group2",record.value().toString());
        //手动提交offset
        ack.acknowledge();
    }

    //配置多个消费组
    @KafkaListener(topics = "my-replicated-topic",groupId = "Group1")
    public void listenTulingGroup(ConsumerRecord<String, String> record, Acknowledgment ack) {
        log.info("组{}自行确认方式收到消息的key: {}" ,"Group1",record.key());
        log.info("组{}自行确认方式收到消息的value: {}","Group1",record.value().toString());
        ack.acknowledge();
    }
}
