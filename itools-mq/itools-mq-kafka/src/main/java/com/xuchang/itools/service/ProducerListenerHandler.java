package com.xuchang.itools.service;


import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.stereotype.Component;


@Component
public class ProducerListenerHandler implements ProducerListener {
    private static final Logger log = LoggerFactory.getLogger(ProducerListenerHandler.class);

    @Override
    public void onSuccess(String s, Integer integer, Object o, Object o2, RecordMetadata recordMetadata) {
        log.info("Message send success : " + o2.toString());
    }

    @Override
    public void onError(String s, Integer integer, Object o, Object o2, Exception e) {
        log.info("Message send error : " + o2.toString());
    }

    @Override
    public boolean isInterestedInSuccess() {
        return false;
    }
}

