package com.xuchang.itools.service;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class KafkaProviderService {
    @Autowired
    private KafkaTemplate kafkaTemplate;

    public void sendMsg() {
        for (int i = 1; i <= 10; i++) {
            kafkaTemplate.send("kafka-spring","spring-key", "hello spring boot");
        }
    }
    public void test2(){
        kafkaTemplate.send("my-replicated-topic","my-replicated-topic-key", "hello spring boot");
    }
    public void test3(){
        kafkaTemplate.send("my-replicated-topic","my-replicated-topic-key", "hello spring boot");
    }

    public void test4(){
        Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", "broker1:9092,broker2:9092");
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(kafkaProps);
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("CustomerCountry",
                "Precision Products", "France");
        try {
            kafkaProducer.send(record);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
