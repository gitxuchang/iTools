package test;

import com.xuchang.itools.KafkaApplication;
import com.xuchang.itools.service.KafkaProviderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
//主application方法
@SpringBootTest(classes= KafkaApplication.class)
public class KafkaTest {
    @Autowired
    private KafkaProviderService kafkaProviderService;
    @Test
    public void mainTest(){
        kafkaProviderService.sendMsg();
    }
}
