package com.xuchang.itools.volatileDemo;

/**
 * 可见性
 */
public class VisibilityTest {

    private volatile   boolean  flag = false;


    public void refresh(){
        this.flag = true;
        System.out.println(Thread.currentThread().getName()+"修改flag");
    }

    public void load(){

        while (!flag){
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }

            //shortWait(100000);

        }
        System.out.println(Thread.currentThread().getName()+"跳出循环: flag="+ flag);
}

    public static void main(String[] args){

        VisibilityTest test = new VisibilityTest();


        new Thread(() -> test.load(), "threadA").start();


        try {
            Thread.sleep(2000);

            new Thread(()->test.refresh(),"threadB").start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



    }


    public static void shortWait(long interval){
        long start = System.nanoTime();
        long end;
        do{
            end = System.nanoTime();
        }while(start + interval >= end);
    }



}
