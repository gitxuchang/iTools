package com.xuchang.itools.test;

import java.util.concurrent.locks.LockSupport;

/**
 * 源码学院-Fox
 * 只为培养BAT程序员而生
 * http://bat.ke.qq.com
 * 往期视频加群:516212256 暗号:6
 * 中断
 */
public class ThreadInterruptTest {

    static int i = 0;

    public static void main(String[] args)  {
        System.out.println("begin");
        Thread t1 = new Thread(new Runnable() {
            @Override
            public synchronized void run() {
                while (true) {
                    i++;
                    System.out.println(i);
                    // sleep 期间可以感知中断信号吗？  可以
                    // stdout(缓存区)  stderr
                    // park 可以被中断 ,不会清除中断标志位
                    LockSupport.park();
//                    try {
//                        // sleep可以被中断 抛出中断异常：sleep interrupted， 清除中断标志位
//                        //Thread.sleep(5000);
//                        // wait可以被中断 抛出中断异常：InterruptedException， 清除中断标志位
//                        wait(5000);
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                    // Thread.interrupted()  清除中断标志位
                    //Thread.currentThread().isInterrupted() 不会清除中断标志位
                    if (Thread.interrupted()) {
                        System.out.println("======");
                        //break;
                    }

                    if(i==10){
                        break;
                    }

                }
            }
        });

        t1.start();
        // 中断方法   不会中断线程t1,只会设置一个中断标志位 flag=true
        //t1.interrupt();
        LockSupport.unpark(t1);



    }
}
