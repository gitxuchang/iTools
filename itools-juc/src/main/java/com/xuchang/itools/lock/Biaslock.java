package com.xuchang.itools.lock;



public class Biaslock {

    private static Object object = new Object();
    /**
     * 默认开启偏向锁
     * 开启偏向锁：-XX:+UseBiasedLocking -XX:BiasedLockingStartupDelay=0
     * 关闭偏向锁：-XX:-UseBiasedLocking
     * @param args
     */
    public static void main(String[] args){
        long begin = System.currentTimeMillis();
        int count = 0;
        while(count < 100000000){
            synchronized (object){
                count++;
            }
        }
        long end = System.currentTimeMillis();
        System.out.println(end - begin);
    }

}
