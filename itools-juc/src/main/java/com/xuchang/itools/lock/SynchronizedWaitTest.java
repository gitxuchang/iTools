package com.xuchang.itools.lock;


public class SynchronizedWaitTest {


    public void test() {
        System.out.println(Thread.currentThread().getId()+" start");
        synchronized (this){
            System.out.println(Thread.currentThread().getId()+" execute");
            try {
                wait(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(Thread.currentThread().getId()+" end");
    }



    public static void main(String[] args) {
        SynchronizedWaitTest test = new SynchronizedWaitTest();

        for(int i=0;i<2;i++){

            new Thread(new Runnable() {
                @Override
                public void run() {
                    test.test();
                }
            }).start();
        }



    }


}
