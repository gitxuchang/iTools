package com.xuchang.itools.threadpool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;


public class ThreadPool01 {
   public static void main(String[] args) {
      ExecutorService executorService = Executors.newCachedThreadPool();
      List<FutureTask<Integer>> taskList = new ArrayList<>();
      for (int i=1;i<10;i++){
         Test1 test1 = new Test1(i*1000);
         FutureTask<Integer> futureTask = new FutureTask(test1);
         taskList.add(futureTask);
         executorService.execute(futureTask);
      }
      for(FutureTask<Integer> task: taskList)
      {
         try {
            if(task.get()==null)
            {
               throw  new Exception("test");
            }
         } catch (Exception e) {
            e.printStackTrace();
         }
      }
      System.out.println("主线程================================");
   }

}
