package com.xuchang.itools.threadpool.sch;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TestThread {
	private static ScheduledExecutorService eService;
	private static long time;

	public static void main(String[] args) {
		time = System.currentTimeMillis();
		eService = Executors.newScheduledThreadPool(3);
		eService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.println(System.currentTimeMillis() - time + "---------" + Thread.currentThread().getName());
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}, 0, 3, TimeUnit.SECONDS);

	}
}