package com.xuchang.itools.threadpool;

import java.util.concurrent.Callable;

public class Test1 implements Callable<Integer> {
    private int time;

    public Test1(int time) {
        this.time = time;
    }

    /**
       * Computes a result, or throws an exception if unable to do so.
       *
       * @return computed result
       * @throws Exception if unable to compute a result
       */
      @Override
      public Integer call() throws Exception {
         Thread.sleep(time);
          System.out.println("子线程");
          return time;
      }
   }