package com.xuchang.itools.threadpool.pk;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ThreadPkTest {
    public static void main(String[] args) throws InterruptedException {
        Long start= System.currentTimeMillis();
        final List<Integer> l=new ArrayList<Integer>();
        final Random random=new Random();
        for(int i=0;i<10000;i++){
            Thread thread=new Thread(){
                @Override
                public void run(){
                    l.add(random.nextInt());
                }
            };
            thread.start();
            thread.join();//这个join的目的???
        }
        System.out.println("时间:"+(System.currentTimeMillis()-start));
        System.out.println("size:"+l.size());

    }
}
