package com.xuchang.itools.blockqueue;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class WorkDesk {
    private BlockingQueue<String> desk = new LinkedBlockingQueue<>(100);
    public void washDish(int i) throws InterruptedException{
//        desk.put("盘子"+i);
        //队满
        int capacity = desk.remainingCapacity();
        if (capacity == 0){
            boolean offer = desk.offer("盘子" + i, 2, TimeUnit.SECONDS);
            if (!offer){
                ArrayList<String> strings = new ArrayList<>();
                desk.drainTo(strings,5);
                System.out.println("队满取出5帧，消费----------使用一个"+strings.get(2));
            }
            return;
        }
        boolean offer = desk.offer("盘子" + i, 4, TimeUnit.SECONDS);
        if (!offer){
            System.out.println("放弃任务");
        }
    }
    public String useDish() throws InterruptedException{
        return desk.take();
    }
}