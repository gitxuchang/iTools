package com.xuchang.itools.blockqueue;

public class Consumer implements Runnable {
    private String consumerName;
    private WorkDesk workDesk;

    public Consumer(String consumerName, WorkDesk workDesk){
        this.consumerName = consumerName;
        this.workDesk = workDesk;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String s1 = workDesk.useDish();
                System.out.println(consumerName + "---------使用一个"+s1);
//                String s2 = workDesk.useDish();
//                System.out.println(consumerName + "---------使用一个"+s2);
//                String s3 = workDesk.useDish();
//                System.out.println(consumerName + "---------使用一个"+s3);
                Thread.sleep(3000);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}