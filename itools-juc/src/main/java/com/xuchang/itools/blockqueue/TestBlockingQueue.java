package com.xuchang.itools.blockqueue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestBlockingQueue {
    public static void main(String[] args){
        WorkDesk workDesk = new WorkDesk();

        ExecutorService service = Executors.newCachedThreadPool();
        Producer producer01 = new Producer("生产者-1-", workDesk);
        Producer producer02 = new Producer("生产者-2-", workDesk);

        Consumer consumer01 = new Consumer("消费者-1-", workDesk);
        Consumer consumer02 = new Consumer("消费者-2-", workDesk);

//        service.submit(producer01);
        service.submit(producer02);
//        service.submit(consumer01);
        service.submit(consumer02);
    }
}