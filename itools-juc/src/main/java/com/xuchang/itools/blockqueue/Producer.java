package com.xuchang.itools.blockqueue;

public class Producer implements Runnable {
    private String producerName;
    private WorkDesk workDesk;

    public Producer(String producerName, WorkDesk workDesk){
        this.producerName = producerName;
        this.workDesk = workDesk;
    }
    @Override
    public void run() {
        try {
            int i = 1;
            while (true) {
                workDesk.washDish(i);
                System.out.println(producerName + "洗好一个盘子"+i);
                ++i;
                workDesk.washDish(i);
                System.out.println(producerName + "洗好一个盘子"+i);
                ++i;
                workDesk.washDish(i);
                System.out.println(producerName + "洗好一个盘子"+i);
                ++i;
//                Thread.sleep(3000);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}