package com.xuchang.itools.rbac.dto;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseDTOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EnterpriseDTOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Long value) {
            addCriterion("CREATE_TIME =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Long value) {
            addCriterion("CREATE_TIME <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Long value) {
            addCriterion("CREATE_TIME >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("CREATE_TIME >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Long value) {
            addCriterion("CREATE_TIME <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Long value) {
            addCriterion("CREATE_TIME <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Long> values) {
            addCriterion("CREATE_TIME in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Long> values) {
            addCriterion("CREATE_TIME not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Long value1, Long value2) {
            addCriterion("CREATE_TIME between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Long value1, Long value2) {
            addCriterion("CREATE_TIME not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdIsNull() {
            addCriterion("CREATE_USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdIsNotNull() {
            addCriterion("CREATE_USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdEqualTo(String value) {
            addCriterion("CREATE_USER_ID =", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdNotEqualTo(String value) {
            addCriterion("CREATE_USER_ID <>", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdGreaterThan(String value) {
            addCriterion("CREATE_USER_ID >", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATE_USER_ID >=", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdLessThan(String value) {
            addCriterion("CREATE_USER_ID <", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdLessThanOrEqualTo(String value) {
            addCriterion("CREATE_USER_ID <=", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdLike(String value) {
            addCriterion("CREATE_USER_ID like", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdNotLike(String value) {
            addCriterion("CREATE_USER_ID not like", value, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdIn(List<String> values) {
            addCriterion("CREATE_USER_ID in", values, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdNotIn(List<String> values) {
            addCriterion("CREATE_USER_ID not in", values, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdBetween(String value1, String value2) {
            addCriterion("CREATE_USER_ID between", value1, value2, "createUserId");
            return (Criteria) this;
        }

        public Criteria andCreateUserIdNotBetween(String value1, String value2) {
            addCriterion("CREATE_USER_ID not between", value1, value2, "createUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIsNull() {
            addCriterion("LAST_UPDATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIsNotNull() {
            addCriterion("LAST_UPDATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME =", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME <>", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeGreaterThan(Long value) {
            addCriterion("LAST_UPDATE_TIME >", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME >=", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLessThan(Long value) {
            addCriterion("LAST_UPDATE_TIME <", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLessThanOrEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME <=", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIn(List<Long> values) {
            addCriterion("LAST_UPDATE_TIME in", values, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotIn(List<Long> values) {
            addCriterion("LAST_UPDATE_TIME not in", values, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeBetween(Long value1, Long value2) {
            addCriterion("LAST_UPDATE_TIME between", value1, value2, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotBetween(Long value1, Long value2) {
            addCriterion("LAST_UPDATE_TIME not between", value1, value2, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdIsNull() {
            addCriterion("LAST_UPDATE_USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdIsNotNull() {
            addCriterion("LAST_UPDATE_USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdEqualTo(String value) {
            addCriterion("LAST_UPDATE_USER_ID =", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdNotEqualTo(String value) {
            addCriterion("LAST_UPDATE_USER_ID <>", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdGreaterThan(String value) {
            addCriterion("LAST_UPDATE_USER_ID >", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_UPDATE_USER_ID >=", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdLessThan(String value) {
            addCriterion("LAST_UPDATE_USER_ID <", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdLessThanOrEqualTo(String value) {
            addCriterion("LAST_UPDATE_USER_ID <=", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdLike(String value) {
            addCriterion("LAST_UPDATE_USER_ID like", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdNotLike(String value) {
            addCriterion("LAST_UPDATE_USER_ID not like", value, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdIn(List<String> values) {
            addCriterion("LAST_UPDATE_USER_ID in", values, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdNotIn(List<String> values) {
            addCriterion("LAST_UPDATE_USER_ID not in", values, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdBetween(String value1, String value2) {
            addCriterion("LAST_UPDATE_USER_ID between", value1, value2, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andLastUpdateUserIdNotBetween(String value1, String value2) {
            addCriterion("LAST_UPDATE_USER_ID not between", value1, value2, "lastUpdateUserId");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumIsNull() {
            addCriterion("EMPLOYEES_NUM is null");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumIsNotNull() {
            addCriterion("EMPLOYEES_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumEqualTo(String value) {
            addCriterion("EMPLOYEES_NUM =", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumNotEqualTo(String value) {
            addCriterion("EMPLOYEES_NUM <>", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumGreaterThan(String value) {
            addCriterion("EMPLOYEES_NUM >", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEES_NUM >=", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumLessThan(String value) {
            addCriterion("EMPLOYEES_NUM <", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEES_NUM <=", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumLike(String value) {
            addCriterion("EMPLOYEES_NUM like", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumNotLike(String value) {
            addCriterion("EMPLOYEES_NUM not like", value, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumIn(List<String> values) {
            addCriterion("EMPLOYEES_NUM in", values, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumNotIn(List<String> values) {
            addCriterion("EMPLOYEES_NUM not in", values, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumBetween(String value1, String value2) {
            addCriterion("EMPLOYEES_NUM between", value1, value2, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEmployeesNumNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEES_NUM not between", value1, value2, "employeesNum");
            return (Criteria) this;
        }

        public Criteria andEnterAddressIsNull() {
            addCriterion("ENTER_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andEnterAddressIsNotNull() {
            addCriterion("ENTER_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andEnterAddressEqualTo(String value) {
            addCriterion("ENTER_ADDRESS =", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressNotEqualTo(String value) {
            addCriterion("ENTER_ADDRESS <>", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressGreaterThan(String value) {
            addCriterion("ENTER_ADDRESS >", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressGreaterThanOrEqualTo(String value) {
            addCriterion("ENTER_ADDRESS >=", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressLessThan(String value) {
            addCriterion("ENTER_ADDRESS <", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressLessThanOrEqualTo(String value) {
            addCriterion("ENTER_ADDRESS <=", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressLike(String value) {
            addCriterion("ENTER_ADDRESS like", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressNotLike(String value) {
            addCriterion("ENTER_ADDRESS not like", value, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressIn(List<String> values) {
            addCriterion("ENTER_ADDRESS in", values, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressNotIn(List<String> values) {
            addCriterion("ENTER_ADDRESS not in", values, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressBetween(String value1, String value2) {
            addCriterion("ENTER_ADDRESS between", value1, value2, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterAddressNotBetween(String value1, String value2) {
            addCriterion("ENTER_ADDRESS not between", value1, value2, "enterAddress");
            return (Criteria) this;
        }

        public Criteria andEnterContactsIsNull() {
            addCriterion("ENTER_CONTACTS is null");
            return (Criteria) this;
        }

        public Criteria andEnterContactsIsNotNull() {
            addCriterion("ENTER_CONTACTS is not null");
            return (Criteria) this;
        }

        public Criteria andEnterContactsEqualTo(String value) {
            addCriterion("ENTER_CONTACTS =", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsNotEqualTo(String value) {
            addCriterion("ENTER_CONTACTS <>", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsGreaterThan(String value) {
            addCriterion("ENTER_CONTACTS >", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsGreaterThanOrEqualTo(String value) {
            addCriterion("ENTER_CONTACTS >=", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsLessThan(String value) {
            addCriterion("ENTER_CONTACTS <", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsLessThanOrEqualTo(String value) {
            addCriterion("ENTER_CONTACTS <=", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsLike(String value) {
            addCriterion("ENTER_CONTACTS like", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsNotLike(String value) {
            addCriterion("ENTER_CONTACTS not like", value, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsIn(List<String> values) {
            addCriterion("ENTER_CONTACTS in", values, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsNotIn(List<String> values) {
            addCriterion("ENTER_CONTACTS not in", values, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsBetween(String value1, String value2) {
            addCriterion("ENTER_CONTACTS between", value1, value2, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterContactsNotBetween(String value1, String value2) {
            addCriterion("ENTER_CONTACTS not between", value1, value2, "enterContacts");
            return (Criteria) this;
        }

        public Criteria andEnterNameIsNull() {
            addCriterion("ENTER_NAME is null");
            return (Criteria) this;
        }

        public Criteria andEnterNameIsNotNull() {
            addCriterion("ENTER_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andEnterNameEqualTo(String value) {
            addCriterion("ENTER_NAME =", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameNotEqualTo(String value) {
            addCriterion("ENTER_NAME <>", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameGreaterThan(String value) {
            addCriterion("ENTER_NAME >", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameGreaterThanOrEqualTo(String value) {
            addCriterion("ENTER_NAME >=", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameLessThan(String value) {
            addCriterion("ENTER_NAME <", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameLessThanOrEqualTo(String value) {
            addCriterion("ENTER_NAME <=", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameLike(String value) {
            addCriterion("ENTER_NAME like", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameNotLike(String value) {
            addCriterion("ENTER_NAME not like", value, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameIn(List<String> values) {
            addCriterion("ENTER_NAME in", values, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameNotIn(List<String> values) {
            addCriterion("ENTER_NAME not in", values, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameBetween(String value1, String value2) {
            addCriterion("ENTER_NAME between", value1, value2, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterNameNotBetween(String value1, String value2) {
            addCriterion("ENTER_NAME not between", value1, value2, "enterName");
            return (Criteria) this;
        }

        public Criteria andEnterTradeIsNull() {
            addCriterion("ENTER_TRADE is null");
            return (Criteria) this;
        }

        public Criteria andEnterTradeIsNotNull() {
            addCriterion("ENTER_TRADE is not null");
            return (Criteria) this;
        }

        public Criteria andEnterTradeEqualTo(String value) {
            addCriterion("ENTER_TRADE =", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeNotEqualTo(String value) {
            addCriterion("ENTER_TRADE <>", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeGreaterThan(String value) {
            addCriterion("ENTER_TRADE >", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeGreaterThanOrEqualTo(String value) {
            addCriterion("ENTER_TRADE >=", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeLessThan(String value) {
            addCriterion("ENTER_TRADE <", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeLessThanOrEqualTo(String value) {
            addCriterion("ENTER_TRADE <=", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeLike(String value) {
            addCriterion("ENTER_TRADE like", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeNotLike(String value) {
            addCriterion("ENTER_TRADE not like", value, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeIn(List<String> values) {
            addCriterion("ENTER_TRADE in", values, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeNotIn(List<String> values) {
            addCriterion("ENTER_TRADE not in", values, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeBetween(String value1, String value2) {
            addCriterion("ENTER_TRADE between", value1, value2, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterTradeNotBetween(String value1, String value2) {
            addCriterion("ENTER_TRADE not between", value1, value2, "enterTrade");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseIsNull() {
            addCriterion("ENTER_LICENSE is null");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseIsNotNull() {
            addCriterion("ENTER_LICENSE is not null");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseEqualTo(String value) {
            addCriterion("ENTER_LICENSE =", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseNotEqualTo(String value) {
            addCriterion("ENTER_LICENSE <>", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseGreaterThan(String value) {
            addCriterion("ENTER_LICENSE >", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseGreaterThanOrEqualTo(String value) {
            addCriterion("ENTER_LICENSE >=", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseLessThan(String value) {
            addCriterion("ENTER_LICENSE <", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseLessThanOrEqualTo(String value) {
            addCriterion("ENTER_LICENSE <=", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseLike(String value) {
            addCriterion("ENTER_LICENSE like", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseNotLike(String value) {
            addCriterion("ENTER_LICENSE not like", value, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseIn(List<String> values) {
            addCriterion("ENTER_LICENSE in", values, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseNotIn(List<String> values) {
            addCriterion("ENTER_LICENSE not in", values, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseBetween(String value1, String value2) {
            addCriterion("ENTER_LICENSE between", value1, value2, "enterLicense");
            return (Criteria) this;
        }

        public Criteria andEnterLicenseNotBetween(String value1, String value2) {
            addCriterion("ENTER_LICENSE not between", value1, value2, "enterLicense");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}