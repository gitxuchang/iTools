package com.xuchang.itools.rbac.dto;

public class UserInfoDTO {
    private String id;

    private Long createTime;

    private String createUserId;

    private String platfromUserid;

    private Long lastUpdateTime;

    private String lastUpdateUserId;

    private String companyId;

    private String organizationId;

    private String name;

    private Short userType;

    private String nodeId;

    private Short sex;

    private String birthday;

    private String email;

    private String position;

    private String remark;

    private String telPhone;

    private String userCode;

    private Short userLevel;

    private String platPersonId;

    private String sign;

    private String briefIntrod;

    private byte[] faceUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getPlatfromUserid() {
        return platfromUserid;
    }

    public void setPlatfromUserid(String platfromUserid) {
        this.platfromUserid = platfromUserid;
    }

    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getUserType() {
        return userType;
    }

    public void setUserType(Short userType) {
        this.userType = userType;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Short getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Short userLevel) {
        this.userLevel = userLevel;
    }

    public String getPlatPersonId() {
        return platPersonId;
    }

    public void setPlatPersonId(String platPersonId) {
        this.platPersonId = platPersonId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getBriefIntrod() {
        return briefIntrod;
    }

    public void setBriefIntrod(String briefIntrod) {
        this.briefIntrod = briefIntrod;
    }

    public byte[] getFaceUrl() {
        return faceUrl;
    }

    public void setFaceUrl(byte[] faceUrl) {
        this.faceUrl = faceUrl;
    }
}