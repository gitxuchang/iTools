package com.xuchang.itools.rbac.param.initDB;

import lombok.Data;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/11 9:57
 */
@Data
public class InitDBInsertParam {
    private String content;
    private String type;
    private String version;
    private String env;
}
