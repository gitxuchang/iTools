package com.xuchang.itools.rbac.dto;

public class EnterpriseDTO {
    private String id;

    private Long createTime;

    private String createUserId;

    private Long lastUpdateTime;

    private String lastUpdateUserId;

    private String employeesNum;

    private String enterAddress;

    private String enterContacts;

    private String enterName;

    private String enterTrade;

    private String enterLicense;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }

    public String getEmployeesNum() {
        return employeesNum;
    }

    public void setEmployeesNum(String employeesNum) {
        this.employeesNum = employeesNum;
    }

    public String getEnterAddress() {
        return enterAddress;
    }

    public void setEnterAddress(String enterAddress) {
        this.enterAddress = enterAddress;
    }

    public String getEnterContacts() {
        return enterContacts;
    }

    public void setEnterContacts(String enterContacts) {
        this.enterContacts = enterContacts;
    }

    public String getEnterName() {
        return enterName;
    }

    public void setEnterName(String enterName) {
        this.enterName = enterName;
    }

    public String getEnterTrade() {
        return enterTrade;
    }

    public void setEnterTrade(String enterTrade) {
        this.enterTrade = enterTrade;
    }

    public String getEnterLicense() {
        return enterLicense;
    }

    public void setEnterLicense(String enterLicense) {
        this.enterLicense = enterLicense;
    }
}