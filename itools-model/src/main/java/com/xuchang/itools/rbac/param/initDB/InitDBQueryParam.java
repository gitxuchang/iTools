package com.xuchang.itools.rbac.param.initDB;

import lombok.Data;

import java.util.Date;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/11 9:57
 */
@Data
public class InitDBQueryParam {
    private String id;
    private String content;
    private String type;
    private String version;
    private Date createTime;
    private String env;
}
