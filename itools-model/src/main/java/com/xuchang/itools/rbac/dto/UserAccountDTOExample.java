package com.xuchang.itools.rbac.dto;

import java.util.ArrayList;
import java.util.List;

public class UserAccountDTOExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserAccountDTOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredIsNull() {
            addCriterion("ACCOUNT_EXPIRED is null");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredIsNotNull() {
            addCriterion("ACCOUNT_EXPIRED is not null");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredEqualTo(Long value) {
            addCriterion("ACCOUNT_EXPIRED =", value, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredNotEqualTo(Long value) {
            addCriterion("ACCOUNT_EXPIRED <>", value, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredGreaterThan(Long value) {
            addCriterion("ACCOUNT_EXPIRED >", value, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredGreaterThanOrEqualTo(Long value) {
            addCriterion("ACCOUNT_EXPIRED >=", value, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredLessThan(Long value) {
            addCriterion("ACCOUNT_EXPIRED <", value, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredLessThanOrEqualTo(Long value) {
            addCriterion("ACCOUNT_EXPIRED <=", value, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredIn(List<Long> values) {
            addCriterion("ACCOUNT_EXPIRED in", values, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredNotIn(List<Long> values) {
            addCriterion("ACCOUNT_EXPIRED not in", values, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredBetween(Long value1, Long value2) {
            addCriterion("ACCOUNT_EXPIRED between", value1, value2, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountExpiredNotBetween(Long value1, Long value2) {
            addCriterion("ACCOUNT_EXPIRED not between", value1, value2, "accountExpired");
            return (Criteria) this;
        }

        public Criteria andAccountLockedIsNull() {
            addCriterion("ACCOUNT_LOCKED is null");
            return (Criteria) this;
        }

        public Criteria andAccountLockedIsNotNull() {
            addCriterion("ACCOUNT_LOCKED is not null");
            return (Criteria) this;
        }

        public Criteria andAccountLockedEqualTo(Short value) {
            addCriterion("ACCOUNT_LOCKED =", value, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedNotEqualTo(Short value) {
            addCriterion("ACCOUNT_LOCKED <>", value, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedGreaterThan(Short value) {
            addCriterion("ACCOUNT_LOCKED >", value, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedGreaterThanOrEqualTo(Short value) {
            addCriterion("ACCOUNT_LOCKED >=", value, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedLessThan(Short value) {
            addCriterion("ACCOUNT_LOCKED <", value, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedLessThanOrEqualTo(Short value) {
            addCriterion("ACCOUNT_LOCKED <=", value, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedIn(List<Short> values) {
            addCriterion("ACCOUNT_LOCKED in", values, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedNotIn(List<Short> values) {
            addCriterion("ACCOUNT_LOCKED not in", values, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedBetween(Short value1, Short value2) {
            addCriterion("ACCOUNT_LOCKED between", value1, value2, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andAccountLockedNotBetween(Short value1, Short value2) {
            addCriterion("ACCOUNT_LOCKED not between", value1, value2, "accountLocked");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Long value) {
            addCriterion("CREATE_TIME =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Long value) {
            addCriterion("CREATE_TIME <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Long value) {
            addCriterion("CREATE_TIME >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("CREATE_TIME >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Long value) {
            addCriterion("CREATE_TIME <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Long value) {
            addCriterion("CREATE_TIME <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Long> values) {
            addCriterion("CREATE_TIME in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Long> values) {
            addCriterion("CREATE_TIME not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Long value1, Long value2) {
            addCriterion("CREATE_TIME between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Long value1, Long value2) {
            addCriterion("CREATE_TIME not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCredentialIsNull() {
            addCriterion("CREDENTIAL is null");
            return (Criteria) this;
        }

        public Criteria andCredentialIsNotNull() {
            addCriterion("CREDENTIAL is not null");
            return (Criteria) this;
        }

        public Criteria andCredentialEqualTo(String value) {
            addCriterion("CREDENTIAL =", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialNotEqualTo(String value) {
            addCriterion("CREDENTIAL <>", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialGreaterThan(String value) {
            addCriterion("CREDENTIAL >", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialGreaterThanOrEqualTo(String value) {
            addCriterion("CREDENTIAL >=", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialLessThan(String value) {
            addCriterion("CREDENTIAL <", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialLessThanOrEqualTo(String value) {
            addCriterion("CREDENTIAL <=", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialLike(String value) {
            addCriterion("CREDENTIAL like", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialNotLike(String value) {
            addCriterion("CREDENTIAL not like", value, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialIn(List<String> values) {
            addCriterion("CREDENTIAL in", values, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialNotIn(List<String> values) {
            addCriterion("CREDENTIAL not in", values, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialBetween(String value1, String value2) {
            addCriterion("CREDENTIAL between", value1, value2, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialNotBetween(String value1, String value2) {
            addCriterion("CREDENTIAL not between", value1, value2, "credential");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredIsNull() {
            addCriterion("CREDENTIAL_EXPIRED is null");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredIsNotNull() {
            addCriterion("CREDENTIAL_EXPIRED is not null");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredEqualTo(Long value) {
            addCriterion("CREDENTIAL_EXPIRED =", value, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredNotEqualTo(Long value) {
            addCriterion("CREDENTIAL_EXPIRED <>", value, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredGreaterThan(Long value) {
            addCriterion("CREDENTIAL_EXPIRED >", value, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredGreaterThanOrEqualTo(Long value) {
            addCriterion("CREDENTIAL_EXPIRED >=", value, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredLessThan(Long value) {
            addCriterion("CREDENTIAL_EXPIRED <", value, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredLessThanOrEqualTo(Long value) {
            addCriterion("CREDENTIAL_EXPIRED <=", value, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredIn(List<Long> values) {
            addCriterion("CREDENTIAL_EXPIRED in", values, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredNotIn(List<Long> values) {
            addCriterion("CREDENTIAL_EXPIRED not in", values, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredBetween(Long value1, Long value2) {
            addCriterion("CREDENTIAL_EXPIRED between", value1, value2, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andCredentialExpiredNotBetween(Long value1, Long value2) {
            addCriterion("CREDENTIAL_EXPIRED not between", value1, value2, "credentialExpired");
            return (Criteria) this;
        }

        public Criteria andIdentifierIsNull() {
            addCriterion("IDENTIFIER is null");
            return (Criteria) this;
        }

        public Criteria andIdentifierIsNotNull() {
            addCriterion("IDENTIFIER is not null");
            return (Criteria) this;
        }

        public Criteria andIdentifierEqualTo(String value) {
            addCriterion("IDENTIFIER =", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotEqualTo(String value) {
            addCriterion("IDENTIFIER <>", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierGreaterThan(String value) {
            addCriterion("IDENTIFIER >", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierGreaterThanOrEqualTo(String value) {
            addCriterion("IDENTIFIER >=", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierLessThan(String value) {
            addCriterion("IDENTIFIER <", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierLessThanOrEqualTo(String value) {
            addCriterion("IDENTIFIER <=", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierLike(String value) {
            addCriterion("IDENTIFIER like", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotLike(String value) {
            addCriterion("IDENTIFIER not like", value, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierIn(List<String> values) {
            addCriterion("IDENTIFIER in", values, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotIn(List<String> values) {
            addCriterion("IDENTIFIER not in", values, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierBetween(String value1, String value2) {
            addCriterion("IDENTIFIER between", value1, value2, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentifierNotBetween(String value1, String value2) {
            addCriterion("IDENTIFIER not between", value1, value2, "identifier");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeIsNull() {
            addCriterion("IDENTITY_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeIsNotNull() {
            addCriterion("IDENTITY_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeEqualTo(Short value) {
            addCriterion("IDENTITY_TYPE =", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotEqualTo(Short value) {
            addCriterion("IDENTITY_TYPE <>", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeGreaterThan(Short value) {
            addCriterion("IDENTITY_TYPE >", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeGreaterThanOrEqualTo(Short value) {
            addCriterion("IDENTITY_TYPE >=", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeLessThan(Short value) {
            addCriterion("IDENTITY_TYPE <", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeLessThanOrEqualTo(Short value) {
            addCriterion("IDENTITY_TYPE <=", value, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeIn(List<Short> values) {
            addCriterion("IDENTITY_TYPE in", values, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotIn(List<Short> values) {
            addCriterion("IDENTITY_TYPE not in", values, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeBetween(Short value1, Short value2) {
            addCriterion("IDENTITY_TYPE between", value1, value2, "identityType");
            return (Criteria) this;
        }

        public Criteria andIdentityTypeNotBetween(Short value1, Short value2) {
            addCriterion("IDENTITY_TYPE not between", value1, value2, "identityType");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpIsNull() {
            addCriterion("LAST_LOGIN_IP is null");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpIsNotNull() {
            addCriterion("LAST_LOGIN_IP is not null");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpEqualTo(String value) {
            addCriterion("LAST_LOGIN_IP =", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotEqualTo(String value) {
            addCriterion("LAST_LOGIN_IP <>", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpGreaterThan(String value) {
            addCriterion("LAST_LOGIN_IP >", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_LOGIN_IP >=", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpLessThan(String value) {
            addCriterion("LAST_LOGIN_IP <", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpLessThanOrEqualTo(String value) {
            addCriterion("LAST_LOGIN_IP <=", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpLike(String value) {
            addCriterion("LAST_LOGIN_IP like", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotLike(String value) {
            addCriterion("LAST_LOGIN_IP not like", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpIn(List<String> values) {
            addCriterion("LAST_LOGIN_IP in", values, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotIn(List<String> values) {
            addCriterion("LAST_LOGIN_IP not in", values, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpBetween(String value1, String value2) {
            addCriterion("LAST_LOGIN_IP between", value1, value2, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotBetween(String value1, String value2) {
            addCriterion("LAST_LOGIN_IP not between", value1, value2, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeIsNull() {
            addCriterion("LAST_LOGIN_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeIsNotNull() {
            addCriterion("LAST_LOGIN_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeEqualTo(String value) {
            addCriterion("LAST_LOGIN_TIME =", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeNotEqualTo(String value) {
            addCriterion("LAST_LOGIN_TIME <>", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeGreaterThan(String value) {
            addCriterion("LAST_LOGIN_TIME >", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_LOGIN_TIME >=", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeLessThan(String value) {
            addCriterion("LAST_LOGIN_TIME <", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeLessThanOrEqualTo(String value) {
            addCriterion("LAST_LOGIN_TIME <=", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeLike(String value) {
            addCriterion("LAST_LOGIN_TIME like", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeNotLike(String value) {
            addCriterion("LAST_LOGIN_TIME not like", value, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeIn(List<String> values) {
            addCriterion("LAST_LOGIN_TIME in", values, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeNotIn(List<String> values) {
            addCriterion("LAST_LOGIN_TIME not in", values, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeBetween(String value1, String value2) {
            addCriterion("LAST_LOGIN_TIME between", value1, value2, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastLoginTimeNotBetween(String value1, String value2) {
            addCriterion("LAST_LOGIN_TIME not between", value1, value2, "lastLoginTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIsNull() {
            addCriterion("LAST_UPDATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIsNotNull() {
            addCriterion("LAST_UPDATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME =", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME <>", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeGreaterThan(Long value) {
            addCriterion("LAST_UPDATE_TIME >", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME >=", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLessThan(Long value) {
            addCriterion("LAST_UPDATE_TIME <", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeLessThanOrEqualTo(Long value) {
            addCriterion("LAST_UPDATE_TIME <=", value, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeIn(List<Long> values) {
            addCriterion("LAST_UPDATE_TIME in", values, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotIn(List<Long> values) {
            addCriterion("LAST_UPDATE_TIME not in", values, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeBetween(Long value1, Long value2) {
            addCriterion("LAST_UPDATE_TIME between", value1, value2, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLastUpdateTimeNotBetween(Long value1, Long value2) {
            addCriterion("LAST_UPDATE_TIME not between", value1, value2, "lastUpdateTime");
            return (Criteria) this;
        }

        public Criteria andLoginModeIsNull() {
            addCriterion("LOGIN_MODE is null");
            return (Criteria) this;
        }

        public Criteria andLoginModeIsNotNull() {
            addCriterion("LOGIN_MODE is not null");
            return (Criteria) this;
        }

        public Criteria andLoginModeEqualTo(Short value) {
            addCriterion("LOGIN_MODE =", value, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeNotEqualTo(Short value) {
            addCriterion("LOGIN_MODE <>", value, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeGreaterThan(Short value) {
            addCriterion("LOGIN_MODE >", value, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeGreaterThanOrEqualTo(Short value) {
            addCriterion("LOGIN_MODE >=", value, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeLessThan(Short value) {
            addCriterion("LOGIN_MODE <", value, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeLessThanOrEqualTo(Short value) {
            addCriterion("LOGIN_MODE <=", value, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeIn(List<Short> values) {
            addCriterion("LOGIN_MODE in", values, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeNotIn(List<Short> values) {
            addCriterion("LOGIN_MODE not in", values, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeBetween(Short value1, Short value2) {
            addCriterion("LOGIN_MODE between", value1, value2, "loginMode");
            return (Criteria) this;
        }

        public Criteria andLoginModeNotBetween(Short value1, Short value2) {
            addCriterion("LOGIN_MODE not between", value1, value2, "loginMode");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Short value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Short value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Short value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Short value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Short value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Short value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Short> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Short> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Short value1, Short value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Short value1, Short value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("USER_ID =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("USER_ID <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("USER_ID >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("USER_ID >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("USER_ID <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("USER_ID <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("USER_ID like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("USER_ID not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("USER_ID in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("USER_ID not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("USER_ID between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("USER_ID not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeIsNull() {
            addCriterion("CREDENTIAL_UPDATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeIsNotNull() {
            addCriterion("CREDENTIAL_UPDATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeEqualTo(Long value) {
            addCriterion("CREDENTIAL_UPDATE_TIME =", value, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeNotEqualTo(Long value) {
            addCriterion("CREDENTIAL_UPDATE_TIME <>", value, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeGreaterThan(Long value) {
            addCriterion("CREDENTIAL_UPDATE_TIME >", value, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("CREDENTIAL_UPDATE_TIME >=", value, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeLessThan(Long value) {
            addCriterion("CREDENTIAL_UPDATE_TIME <", value, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeLessThanOrEqualTo(Long value) {
            addCriterion("CREDENTIAL_UPDATE_TIME <=", value, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeIn(List<Long> values) {
            addCriterion("CREDENTIAL_UPDATE_TIME in", values, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeNotIn(List<Long> values) {
            addCriterion("CREDENTIAL_UPDATE_TIME not in", values, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeBetween(Long value1, Long value2) {
            addCriterion("CREDENTIAL_UPDATE_TIME between", value1, value2, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andCredentialUpdateTimeNotBetween(Long value1, Long value2) {
            addCriterion("CREDENTIAL_UPDATE_TIME not between", value1, value2, "credentialUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeIsNull() {
            addCriterion("FIRST_PWD_ERROR_TIME is null");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeIsNotNull() {
            addCriterion("FIRST_PWD_ERROR_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeEqualTo(Long value) {
            addCriterion("FIRST_PWD_ERROR_TIME =", value, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeNotEqualTo(Long value) {
            addCriterion("FIRST_PWD_ERROR_TIME <>", value, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeGreaterThan(Long value) {
            addCriterion("FIRST_PWD_ERROR_TIME >", value, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeGreaterThanOrEqualTo(Long value) {
            addCriterion("FIRST_PWD_ERROR_TIME >=", value, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeLessThan(Long value) {
            addCriterion("FIRST_PWD_ERROR_TIME <", value, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeLessThanOrEqualTo(Long value) {
            addCriterion("FIRST_PWD_ERROR_TIME <=", value, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeIn(List<Long> values) {
            addCriterion("FIRST_PWD_ERROR_TIME in", values, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeNotIn(List<Long> values) {
            addCriterion("FIRST_PWD_ERROR_TIME not in", values, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeBetween(Long value1, Long value2) {
            addCriterion("FIRST_PWD_ERROR_TIME between", value1, value2, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andFirstPwdErrorTimeNotBetween(Long value1, Long value2) {
            addCriterion("FIRST_PWD_ERROR_TIME not between", value1, value2, "firstPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeIsNull() {
            addCriterion("LOGIN_PWD_ERROR_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeIsNotNull() {
            addCriterion("LOGIN_PWD_ERROR_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeEqualTo(Short value) {
            addCriterion("LOGIN_PWD_ERROR_TIME =", value, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeNotEqualTo(Short value) {
            addCriterion("LOGIN_PWD_ERROR_TIME <>", value, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeGreaterThan(Short value) {
            addCriterion("LOGIN_PWD_ERROR_TIME >", value, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeGreaterThanOrEqualTo(Short value) {
            addCriterion("LOGIN_PWD_ERROR_TIME >=", value, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeLessThan(Short value) {
            addCriterion("LOGIN_PWD_ERROR_TIME <", value, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeLessThanOrEqualTo(Short value) {
            addCriterion("LOGIN_PWD_ERROR_TIME <=", value, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeIn(List<Short> values) {
            addCriterion("LOGIN_PWD_ERROR_TIME in", values, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeNotIn(List<Short> values) {
            addCriterion("LOGIN_PWD_ERROR_TIME not in", values, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeBetween(Short value1, Short value2) {
            addCriterion("LOGIN_PWD_ERROR_TIME between", value1, value2, "loginPwdErrorTime");
            return (Criteria) this;
        }

        public Criteria andLoginPwdErrorTimeNotBetween(Short value1, Short value2) {
            addCriterion("LOGIN_PWD_ERROR_TIME not between", value1, value2, "loginPwdErrorTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}