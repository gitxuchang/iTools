package com.xuchang.itools.rbac.dto;

public class UserAccountDTO {
    private String id;

    private Long accountExpired;

    private Short accountLocked;

    private Long createTime;

    private String credential;

    private Long credentialExpired;

    private String identifier;

    private Short identityType;

    private String lastLoginIp;

    private String lastLoginTime;

    private Long lastUpdateTime;

    private Short loginMode;

    private Short status;

    private String userId;

    private Long credentialUpdateTime;

    private Long firstPwdErrorTime;

    private Short loginPwdErrorTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAccountExpired() {
        return accountExpired;
    }

    public void setAccountExpired(Long accountExpired) {
        this.accountExpired = accountExpired;
    }

    public Short getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(Short accountLocked) {
        this.accountLocked = accountLocked;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public Long getCredentialExpired() {
        return credentialExpired;
    }

    public void setCredentialExpired(Long credentialExpired) {
        this.credentialExpired = credentialExpired;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Short getIdentityType() {
        return identityType;
    }

    public void setIdentityType(Short identityType) {
        this.identityType = identityType;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Short getLoginMode() {
        return loginMode;
    }

    public void setLoginMode(Short loginMode) {
        this.loginMode = loginMode;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getCredentialUpdateTime() {
        return credentialUpdateTime;
    }

    public void setCredentialUpdateTime(Long credentialUpdateTime) {
        this.credentialUpdateTime = credentialUpdateTime;
    }

    public Long getFirstPwdErrorTime() {
        return firstPwdErrorTime;
    }

    public void setFirstPwdErrorTime(Long firstPwdErrorTime) {
        this.firstPwdErrorTime = firstPwdErrorTime;
    }

    public Short getLoginPwdErrorTime() {
        return loginPwdErrorTime;
    }

    public void setLoginPwdErrorTime(Short loginPwdErrorTime) {
        this.loginPwdErrorTime = loginPwdErrorTime;
    }
}