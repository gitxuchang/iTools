package com.xuchang.itools.rbac.initDB;

import lombok.Data;

import java.util.Date;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/11 9:57
 */
@Data
public class InitDBResultDTO {
    /**
     * id
     */
    private String id;
    /**
     * 执行的sql
     */
    private String content;
    /**
     * 类型，ddl,dml,function
     */
    private String type;
    /**
     * 版本
     */
    private String version;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 分支，环境
     */
    private String env;
}
