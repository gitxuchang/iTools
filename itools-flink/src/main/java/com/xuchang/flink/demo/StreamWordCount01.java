package com.xuchang.flink.demo;

import com.xuchang.flink.constant.ProjectConstant;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-11-01 17:40
 */
public class StreamWordCount01 {
    public static void main(String[] args) throws Exception{
        // 创建流处理执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        // 从文件中读取数据
//        String inputPath = ProjectConstant.PROJECT_PATH+"src\\main\\resources\\hello2.txt";
        String inputPath = "D:\\data\\iTools\\itools-flink\\src\\main\\resources\\hello2.txt";
        DataStream<String> inputDataStream = env.readTextFile(inputPath);

        // 基于数据流进行转换计算
        DataStream<Tuple2<String, Integer>> resultStream = inputDataStream.flatMap(new WordCount.MyFlatMapper()).slotSharingGroup("green")
                .keyBy(0)
                .sum(1);

        resultStream.print();
        // 执行任务
        env.execute();
    }
}
