package com.xuchang.flink.demo;

import com.xuchang.flink.apitest.beans.SensorReading;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer09;
import org.apache.flink.streaming.util.serialization.KeyedSerializationSchema;
import org.apache.flink.util.OutputTag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-11-04 09:03
 */
public class FlinkTest {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(5);
        // 指定处理时间
        // ProcessingTime是以operator处理的时间为准，它使用的是机器的系统时间来作为data stream的时间
        // IngestionTime 是以数据进入flink streaming data flow的时间为准
        // EventTime 是以数据自带的时间戳字段为准，应用程序需要指定如何从record中抽取时间戳字段
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        //100ms如果采用EventTime的话，Flink默认的周期是每200ms分配1次WaterMark，如果想要自定义的修改，可以再次执行以下这行语句。
        env.getConfig().setAutoWatermarkInterval(100);

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("group.id", "consumer-group");
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("auto.offset.reset", "latest");

        //测试数据：sensor_1,1547718212,37.1
        //.\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic sinktest2
        //.\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic sensor
        // 从kafka读取数据
        DataStream<String> inputStream = env.addSource( new FlinkKafkaConsumer011<String>("sensor", new SimpleStringSchema(), properties));

        // 转换成SensorReading类型，分配时间戳和watermark
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
                    String[] fields = line.split(",");
                    return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
                });

        // 1. 分流，按照温度值30度为界分为两条流
//        SplitStream<SensorReading> splitStream = dataStream.split(new OutputSelector<SensorReading>() {
//            @Override
//            public Iterable<String> select(SensorReading value) {
//                return (value.getTemperature() > 30) ? Collections.singletonList("high") : Collections.singletonList("low");
//            }
//        });
//        DataStream<SensorReading> highTempStream = splitStream.select("high");
//        DataStream<SensorReading> lowTempStream = splitStream.select("low");
//        DataStream<SensorReading> allTempStream = splitStream.select("high", "low");

        SplitStream<SensorReading> splitStream = dataStream.split(new OutputSelector<SensorReading>() {
            @Override
            public Iterable<String> select(SensorReading sensorReading) {
                List<String> tags = new ArrayList<>();
                String type = sensorReading.getId();
                if (type.equals("sensor_1")) {
                    tags.add("sensor_1");
                } else if (type.equals("sensor_10")) {
                    tags.add("sensor_10");
                }else {
                    tags.add("sensor");
                }
                return tags;
            }
        });
        DataStream<SensorReading> stream1 = splitStream.select("sensor_1");
        DataStream<SensorReading> stream2 = splitStream.select("sensor_10");
        DataStream<SensorReading> stream3 = splitStream.select("sensor");

        stream1.print("sensor_1分组");
        stream2.print("sensor_10分组");
        stream3.print("sensor分组");
        // 乱序数据设置时间戳和watermark
        SingleOutputStreamOperator<SensorReading> sensorReadingSingleOutputStreamOperator =
                dataStream.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<SensorReading>(Time.seconds(2)) {
            @Override
            public long extractTimestamp(SensorReading element) {
                //毫秒
                return element.getTimestamp() * 1000L;
            }
        });
        //侧边输出流
        OutputTag<SensorReading> outputTag = new OutputTag<SensorReading>("late") {};

//        KeyedStream<SensorReading, Tuple> sensorReadingTupleKeyedStream = sensorReadingSingleOutputStreamOperator.keyBy("id");
        KeyedStream<SensorReading, String> sensorReadingTupleKeyedStream = sensorReadingSingleOutputStreamOperator.keyBy(new KeySelector<SensorReading, String>() {
            @Override
            public String getKey(SensorReading sensorReading) throws Exception {
                return sensorReading.getId();
            }
        }, TypeInformation.of(String.class));

//        sensorReadingTupleKeyedStream.print("根据id分组");
        // 基于事件时间的开窗聚合，统计3秒内温度的最小值
        SingleOutputStreamOperator<SensorReading> minTempStream = sensorReadingTupleKeyedStream
                .timeWindow(Time.seconds(3))
                //运行迟到时间
                .allowedLateness(Time.seconds(5))
                //侧边数据输出流
                .sideOutputLateData(outputTag)
                .minBy("temperature");

        //对象序列化成字符串
        DataStream<String> kafkaStream = dataStream.map(sensorReading -> {
            return sensorReading.toString();
        });
        //添加kafka的接收数据源
        kafkaStream.addSink(new FlinkKafkaProducer011<String>("localhost:9092", "sinktest2", new SimpleStringSchema()));

        // 打印输出
        dataStream.print("kafka数据来源");
        minTempStream.print("minTemp");
        minTempStream.getSideOutput(outputTag).print("late");


        env.execute();
    }

}
