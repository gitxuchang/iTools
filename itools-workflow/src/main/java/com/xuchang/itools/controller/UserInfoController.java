package com.xuchang.itools.controller;

import com.xuchang.itools.log.Logable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2020-12-26 22:12
 */
@RestController
@RequestMapping("/user")
public class UserInfoController {

    @GetMapping("/test")
    @Logable(loggerName = "workflow",outputResult=false)
    public String test(){
        return "hello world";
    }
}
