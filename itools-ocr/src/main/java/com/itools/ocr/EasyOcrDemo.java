package com.itools.ocr;

import cn.easyproject.easyocr.EasyOCR;
import cn.easyproject.easyocr.ImageType;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-09-26 15:35
 */
public class EasyOcrDemo {
    public static void main(String[] args) {
        test01();
    }
    private static void test01(){
        EasyOCR e=new EasyOCR();
        //直接识别图片内容
        System.out.println(e.discern("D:\\data\\iTools\\itools-ocr\\000005.jpg"));
        System.out.println(e.discern("D:\\data\\iTools\\itools-ocr\\000004.jpg"));
        System.out.println(e.discern("D:\\data\\iTools\\itools-ocr\\000003.jpg"));
    }

    private static void test02(){
        EasyOCR ocr1=new EasyOCR();
        EasyOCR ocr2=new EasyOCR();
        //直接识别验证码图片内容
        //验证码图片，经过：普通清理、形变场景自动一体化处理后，识别内容
        System.out.println(ocr1.discernAndAutoCleanImage("D:\\data\\iTools\\itools-ocr\\examples\\img_INTERFERENCE_LINE.png", ImageType.CAPTCHA_INTERFERENCE_LINE));
        System.out.println(ocr2.discernAndAutoCleanImage("D:\\data\\iTools\\itools-ocr\\examples\\img_NORMAL.jpg", ImageType.CAPTCHA_NORMAL, 1.6, 0.7));
//        System.out.println(e.recognizeAutoCleanImage("images/img_INTERFERENCE_LINE.png", ImageType.CAPTCHA_INTERFERENCE_LINE));
//        System.out.println(e.recognizeAutoCleanImage("images/img_NORMAL.jpg", ImageType.CAPTCHA_NORMAL, 1.6, 0.7));
    }

//    private static void test03(){
//        EasyOCR ocr = new EasyOCR();
//
//        System.out.println("###### 中文会议通知内容识别 ######");
//        ocr.setAmendPath("amend_chi.txt"); // 中文识别修正
//        ocr.setLanguage(Language.CHI_SIM); // 中文语言
//        String res=ocr.recognize("images/bank/notice.tif");
//        System.out.println(res);
//
//        System.out.println("###### 多语言混合识别 ######");
//        ocr.setLanguage(Language.multiLanguage(Language.ENG,Language.CHI_SIM)); // 多语言识别
//        String res2=ocr.recognize("images/bank/bill2.tif");
//        System.out.println(res2);
//
//        System.out.println("###### 基于ETD模板的中文银行票据识别 ######");
//        ocr.setLanguage(Language.CHI_SIM); // 中文识别
//        ocr.setTextMode(TextMode.UNIFORM_TEXT); // 统一大小
//        List<String> res3=ocr.recognizeByTemplate("images/bank/bill3.jpg", "images/bank/bill.etd", ImageType.BILL_NORMAL);
//        System.out.println(res3);
//
//        System.out.println("###### 带图片的清理数字内容识别 ######");
//        ocr.setLanguage(Language.ENG); // 英文识别
//        ocr.setCharList("0123456789"); // 字符限定API
//        ocr.setTextMode(TextMode.SINGLE_LINE_TEXT); // 单行文本识别
//        String res4=ocr.recognizeAutoCleanImage("images/bank/example4.jpg",ImageType.TEXT_BOLD_BLAK);
//        System.out.println(res4);
//    }
}
