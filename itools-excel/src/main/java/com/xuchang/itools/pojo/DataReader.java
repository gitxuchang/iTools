package com.xuchang.itools.pojo;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import sun.rmi.runtime.Log;

import java.util.Map;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2021-06-26 17:07
 */
@Slf4j
public class DataReader extends AnalysisEventListener<ExcelMode> {

    @Override
    public void invoke(ExcelMode excelMode, AnalysisContext analysisContext) {
        try {
            log.info("解析到一条数据:{}", JSON.toJSONString(excelMode));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        log.info("头部信息{}", JSON.toJSONString(headMap));
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

}
