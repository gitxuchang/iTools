package com.xuchang.itools.code;

import com.xuchang.itools.base.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("system")
public class SystemCodeController {

    private final SystemCodeService systemCodeService;

    public SystemCodeController(@Autowired SystemCodeService systemCodeService) {
        this.systemCodeService = systemCodeService;
    }

    @RequestMapping(value = "translate/{code}", method = RequestMethod.GET)
    public CommonResult<Map<String, String>> translate(@PathVariable(value = "code") String code){
        Map<String, String> result = new HashMap<>(2);
        result.put("code", code);
        result.put("msg", systemCodeService.getMessageOptional(code).orElse(""));
        return CommonResult.success(result);
    }

    @RequestMapping(value = "translate/codes", method = RequestMethod.GET)
    public CommonResult<Map<String, String>> codes(){
        return CommonResult.success(systemCodeService.getSystemCodeMap());
    }
}
