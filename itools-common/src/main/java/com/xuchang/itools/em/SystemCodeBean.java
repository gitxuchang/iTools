package com.xuchang.itools.em;


import com.xuchang.itools.code.SystemCode;

@SystemCode
public class SystemCodeBean {

    public enum SystemCode {
        /**
         * 需要在配置文件指定
         * system:
         *   errorCode: Code001
         */
        SYSTEM_EXCEPTION("Code001" , "系统内部错误"),
        RETE_EXCEPTION("Code002" , "接口请求频繁，请稍后重试！"),


        ;
        private final String code;
        private final String message;

        SystemCode(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }

    }
}
