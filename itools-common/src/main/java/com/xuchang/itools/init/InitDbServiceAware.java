package com.xuchang.itools.init;

import com.xuchang.itools.rbac.param.initDB.InitDBInsertParam;
import com.xuchang.itools.rbac.param.initDB.InitDBQueryParam;
import com.xuchang.itools.rbac.param.initDB.InitDBResult;
import com.xuchang.itools.system.AbstractService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @project: iTools
 * @description:
 * @author: XUCHANG
 * @create: 2020-09-28 22:52
 */
@Service
public class InitDbServiceAware extends AbstractService implements InitDbService {

    @Override
    public List<InitDBResult> query(InitDBQueryParam dbQuery) {
        return new ArrayList<>();
    }

    @Override
    public Integer multipartInsert(List<InitDBInsertParam> dbInsertParams) {
        return null;
    }
}
