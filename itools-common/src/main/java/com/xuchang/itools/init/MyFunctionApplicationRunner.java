package com.xuchang.itools.init;

import com.xuchang.itools.rbac.param.initDB.InitDBInsertParam;
import com.xuchang.itools.rbac.param.initDB.InitDBQueryParam;
import com.xuchang.itools.rbac.param.initDB.InitDBResult;
import com.xuchang.itools.utils.StringUtils;
import org.apache.ibatis.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/10 15:33
 */
@Component
@Order(value = 0)
public class MyFunctionApplicationRunner implements ApplicationRunner {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private Environment environment;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (!environment.containsProperty("initDB.is-use")){
            return;
        }
        Boolean isUse = environment.getProperty("initDB.is-use",Boolean.class);
        if (!isUse){
            return;
        }
        DataSource dataSource = applicationContext.getBean(DataSource.class);
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            logger.info("数据源连接失败",e);
        }
        InitDbService initDBService = applicationContext.getBean(InitDbService.class);
        String functionProperty = environment.getProperty("initDB.function");
        String[] files = functionProperty.split(",");
        MyScriptRunner runner = new MyScriptRunner(connection,getInitDBMap(initDBService));
        try {
            List<InitDBInsertParam> dbInsertParams = new ArrayList<>();
            for (String file : files){
                logger.info("开始执行{}脚本文件",file);
                Reader functionResourceAsReader = Resources.getResourceAsReader(file);
                runner.setDelimiter(environment.getProperty("initDB.delimiter"));
                runner.setImplemented(true);
                runner.setThrowWarning(true);
                List<String> result = runner.runLineScript(functionResourceAsReader);

                for (String sql : result){
                    if (StringUtils.isEmpty(sql)){
                        continue;
                    }
                    InitDBInsertParam initDBInsertParam = new InitDBInsertParam();
                    initDBInsertParam.setContent(sql);
                    initDBInsertParam.setType("function");
                    logger.info("function：执行了"+sql);
                    initDBInsertParam.setEnv(environment.getProperty("initDB.env"));
                    initDBInsertParam.setVersion(environment.getProperty("initDB.version"));
                    dbInsertParams.add(initDBInsertParam);
                }
                logger.info("脚本{}执行成功",file);
            }
            initDBService.multipartInsert(dbInsertParams);
        } catch (IOException e) {
            logger.info("Resources获取失败",e);
        }
        logger.info("FUNCTION脚本执行完成");
    }
    private Map<String,String> getInitDBMap(InitDbService initDBService){
        Map<String,String> result  = new HashMap<>();
        InitDBQueryParam initDBQueryParam = new InitDBQueryParam();
        initDBQueryParam.setEnv(environment.getProperty("initDB.env"));
//        initDBQueryParam.setVersion(environment.getProperty("initDB.version"));
        initDBQueryParam.setType("function");
        List<InitDBResult> resultList = initDBService.query(initDBQueryParam);
        for (InitDBResult initDBResult : resultList){
            result.put(initDBResult.getId(),replaceBlank(initDBResult.getContent()));
        }
        return result;
    }
    private static String replaceBlank(String str) {
        String dest = "";
        if (str!=null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }
}
