package com.xuchang.itools.init;


import com.xuchang.itools.rbac.param.initDB.InitDBInsertParam;
import com.xuchang.itools.rbac.param.initDB.InitDBQueryParam;
import com.xuchang.itools.rbac.param.initDB.InitDBResult;

import java.util.List;

/**
 * @Author XUCHANG
 * @description:
 * @Date 2020/9/11 9:56
 */
public interface InitDbService {

    List<InitDBResult> query(InitDBQueryParam dbQuery);

    Integer multipartInsert(List<InitDBInsertParam> dbInsertParams);
}
