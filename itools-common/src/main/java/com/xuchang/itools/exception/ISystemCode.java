package com.xuchang.itools.exception;
/**
 * 所有AppException使用的各种异常枚举定义均应该实现此接口
 *
 */
public interface ISystemCode {

	String getCode();

	String getMessage();

}
