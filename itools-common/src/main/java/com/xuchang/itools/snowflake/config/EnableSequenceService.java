package com.xuchang.itools.snowflake.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;


@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(SequenceAutoConfiguration.class)
public @interface EnableSequenceService {

}
