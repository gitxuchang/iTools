package com.xuchang.test;

/**
 * @project: dataStructure
 * @description:
 * @author: XUCHANG
 * @create: 2021-03-25 09:23
 */
public class MergeSortTest2 {
    public static void main(String[] args) {
        int[] arr = new int[]{1,5,4,2,3};
        print(arr);
        mergeSort(arr);
        print(arr);
    }

    private static void mergeSort(int[] arr) {
        sort(arr,0,arr.length-1);
    }

    private static void sort(int[] arr, int left, int right) {
        if (left>=right){
            return;
        }
        int mid = (left+right)/2;
        sort(arr,left,mid);
        sort(arr,mid+1,right);
        merge(arr,left,mid,right);
    }

    private static void merge(int[] arr, int left, int mid, int right) {
        int[] tempArr = new int[arr.length];
        int m = mid+1;
        int index = left;
        int temp = left;
        while (left<=mid && m<=right){
            if (arr[left] <= arr[m]){
                tempArr[index++] = arr[left++];
            }else {
                tempArr[index++] = arr[m++];
            }
        }
        while (m<=right){
            tempArr[index++] = arr[m++];
        }
        while (left<=mid){
            tempArr[index++] = arr[left++];
        }
        while (temp<=right){
            arr[temp] = tempArr[temp++];
        }
    }
    public static void print(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + "\t");
        }
        System.out.println();
    }
}
