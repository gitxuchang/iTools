package com.xuchang.LeetCode;

/**
 * @project: dataStructure
 * @description:
 * @author: XUCHANG
 * @create: 2021-05-17 15:39
 */
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class TreeNodeSolution1 {
    private int x_depth = 0;
    private int y_depth = 0;
    private TreeNode x_father = null;
    private TreeNode y_father = null;

    public boolean isCousins(TreeNode root, int x, int y) {
        //深度是否一样
        //是否同一个祖父
        dfs(root,x,y,0);
        return x_depth == y_depth && x_father != y_father;
    }
    public void dfs(TreeNode root, int x, int y, int depth) {
        if(root == null) {
            return ;
        }
        //左
        if(root.left != null) {
            if(root.left.val == x) {
                x_depth = depth+1;
                x_father = root;
            }
            if(root.left.val == y) {
                y_depth = depth+1;
                y_father = root;
            }
            dfs(root.left,x,y,depth+1);
        }
        //右
        if(root.right != null) {
            if(root.right.val == x) {
                x_depth = depth+1;
                x_father = root;
            }
            if(root.right.val == y) {
                y_depth = depth+1;
                y_father = root;
            }
            dfs(root.right,x,y,depth+1);
        }
    }

}
