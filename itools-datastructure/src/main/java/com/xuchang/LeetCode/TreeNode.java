package com.xuchang.LeetCode;

/**
 * @project: dataStructure
 * @description:
 * @author: XUCHANG
 * @create: 2021-05-17 15:40
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
