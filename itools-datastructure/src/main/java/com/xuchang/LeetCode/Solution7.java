package com.xuchang.LeetCode;

class Solution7 {
    public int removeDuplicates(int[] nums) {

        int index = 0;
        for(int i = 0;i < nums.length;i++){
            // 指针运动条件
            if(index == 0 || nums[index-1] != nums[i]){
                nums[index] = nums[i];
                index++;
            }
        }
        return index;
    }
    public int removeDuplicates2(int[] nums) {
        if(nums.length<=2){
            return nums.length;
        }
        int index = 2;
        for(int i = 2;i < nums.length;i++){
            // 指针运动条件
            if(nums[index-2] != nums[i]){
                nums[index++] = nums[i];
            }
        }
        return index;
    }
}