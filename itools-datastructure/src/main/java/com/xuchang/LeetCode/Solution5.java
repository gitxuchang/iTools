package com.xuchang.LeetCode;

/**
 * @project: dataStructure
 * @description:
 * @author: XUCHANG
 * @create: 2021-02-27 11:17
 */
public class Solution5 {
    public static void main(String[] args) {
        Solution5 test = new Solution5();
        int i = test.longestSubstring("ababbc", 2);
        System.out.println(i);
    }
    public int longestSubstring(String s, int k) {
        int res = 0;

        for(int i=0;i<s.length();i++){
            char temp = s.charAt(i);
            int sum = 0;
            for(int j=i;j<s.length();j++){
                if(temp == s.charAt(j)){
                    sum = sum + 1;
                }
            }
            if(sum<k){
                continue;
            }
            res = sum + res;
        }
        return res;
    }
}
